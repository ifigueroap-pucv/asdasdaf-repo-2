# Glosario  

  a.- **Control de versiones (VC)**: es un sistema que registra los cambios a traves del tiempo de un archivo o conjunto de archivos. En este caso el control de versiones centralizado nos permite colaborar con desarrolladores que se encuentran trabajando en otros sistemas. Este sistema tiene un único servidor que contiene todas las versiones del archivo, permitiendo que varios clientes puedan descargar las distintas versiones desde ese servidor central.  
  Posee ventajas como permitir que los distintos desarroladores puedan saber en que etan trabajand los otros colaboradores del proyecto, haciendo mucho mas facil la administración de este, y tambien posee desventajas como una posible caida de ese único servidor nadie podria trabajar en el proyecto dificultando el avance o si se corrompe el disco al estar guardado la informacion en un unico lugar tiene el riesgo de perderlo todo.  
  [Referencia: control de versiones VC][Link A]
  
  b.-**Control de versiones distribuido (DVC)**: este sistema nos permite no solo descargar la ultima actualizacion del archivos sino que replicarla completamente, a diferencia del CVC si un servidor muere y los sitemas colaboran a través de él, cualquiera de los repositorios de los clientes puede copiarse para restaurarlo.  
  [Referencia: control de versiones DVC][Link B]  
  
  c.-**Repositorio remoto y Repositorio local**: un repositorio remoto son aquellas versiones de tu proyecto que se encuentran alojadas en internet o en algun punto de red, esto nos permite colaborar con otros gestionando nuestros repositorios en la red  y mandar mandar (push) o recibir (pull)datos  cuando se necesite compartir la informacion con otros.  
  En cambio un repositorio local es aquel que se encuentra guardado en tu PC, al iniciar un repositorio local se creara un directorio oculto.git en la carpeta seleccionada, que tendra toda la información del control de versiones.  
  [Referencia: repositorio remoto][Link C1]  
  [Referencia: repositorio local][Link C2]  
  
  d.-**Copia de trabajo**: todos los archivos,incluidas sus versiones anteriores, se encuentran guardadas en un repositorio, esto permite que todos los cambios que se han hecho al repositorio siempre estan disponibles para los colaboradores,la gracia de esto es que la copia de trabajo local de cada usuario (un "clon" del comando "git clone") representa un repositorio completo y localizado. Por lo tanto, hay varias copias del repositorio y cualquier persona con un clon puede trabajar en él, organizando cuando y quien hizo los cambios al repositorio.  
  [Referencia: copia de trabajo][Link D]  
  
  e.-**Área de preparación**: el área de preparación es un sencillo archivo, generalmente contenido en tu directorio de Git, que almacena información acerca de lo que va a ir en tu próxima confirmación. A veces se le denomina índice.Una vez confirmados los cambios se sacan tal cual estan del área de preparación y se almacenan permanentemente en el directorio git.  
  [Referencia : área de preparación][Link E]  
  
  f.-**Preparar cambios**: para preparar cambios primero se debe hacer un seguimiento del archivo que se desea cambiar,mediante el uso del comando __*git add*__. Al realizar esta acción el archivo estará bajo seguimiento y preparado, puedes confirmarlo mediante el comando ***git status*** (aparece el mensaje changes to be committed),si confirmas el cambio la version del archivo al momento de ejecutar __*git add*__ será la que se guarde en el repositorio.  
  [Referencia preparar cambios][Link F]  
  
  g.-**Confirmar cambios**: al estar lista el area de preparación se pueden confirmar los cambios, recordar que cualquier modificación o creación que este sin preparar,sobre el que no se haya ejecutado ***git add*** no se incluirá en la confirmación, se mantendran almacenados en tu disco. La forma mas fácil de confirmar los cambios es con el comando ***git commit***.  
  [Referencia: confirmar cambios][Link G]  
  
  h.-**Commit**: este comando se usa para guardar los cambios en el repositorio local. El archivo no se guardará instantaneamente, primero se deben preparar y seleccionar los cambios mediante el comando ***git add***.El uso del comando ***git commit*** solo guarda un nuevo objeto de *commit* en el repositorio local de Git. El intercambio de confirmaciones al repositorio remoto se debe realizar de forma manual y explícita (con los comandos ***git fetch***, ***git pull*** y ***git push***).  
  [Referencia: commit][Link H]  
  
  i.-**Clone**: se utiliza para obtener un proyecto en el cual uno desea contribuir desde un servidor externo, clonando el repositorio existente desde ese servidor.  
  Al usar el comando ***git clone** se crea un directorio llamado "grit", inicializa un directorio .git en su interior, descarga toda la información de ese repositorio, y saca una copia de trabajo de la última versión. Si te metes en el nuevo directorio grit, verás que están los archivos del proyecto, listos para ser utilizados.
  [Referencia: clone][Link I]  
  
  j.-**Pull**: se utiliza para recibir datos de un repositorio remoto, el comando ***git pull*** recupera y une automáticamente la rama remoto con tu rama actual. Al ejecutar ***git pull***, por lo general se recupera la información del servidor del que clonaste, y automáticamente se intenta unir con el código con el que estás trabajando actualmente.  
  [Referencia pull][Link J]  
  
  k.-**Push**: cuando tu proyecto se encuentra en un estado que quieres compartir, tienes que enviarlo a un repositorio remoto. El comando que te permite hacer esto es ___git push___. Este comando funciona unicamente si hss clonado el repositorio en el que tiene spermiso de escritura, si un colaborador envia la información al mismo tiempo tu petición sra rechazada hasta que se agregue el nuevo cambio y enviar tus datos con el cambio de los 2 colaboradores ya agregado al servidor.  
  [Referencia: push][Link K] 
  
  l.-**Fetch**: fetch se utiliza para recuperar datos de tus repositorios remotos, mediante el comando ***git fetch [repositorio]*** puedes obtener los datos del proyecto que aún no tengas. Es importante tener en cuenta que el comando fetch sólo recupera la información y la pone en tu repositorio local,no la une automáticamente con tu trabajo ni modifica aquello en lo que estás trabajando, sino que tendrás que unir ambos de forma manual posteriormente.  
  [Referencia: fetch][Link L]  
  
  m.-**Merge**: merge se usa comunmente para unir dos ramas de trabajo.La rama actual se actualizará para reflejar la fusión, pero la rama de destino no se verá afectada por completo, este comando nos permite unir las lineas independientes creadas con ***git branch*** y fusionarlas en una sola rama.  
  [Referencia: merge][Link M]  
  
  
  n.-**Status**: nos permite ver en el estado que se encuentran los archivos del repositorio (bajo seguimiento , modificados, directorio de trabajo limpio), también este comando nos dice en que rama de trabajo nos encontramos. El comando ***git status*** es importante al momento de preparar nuestros archivos para ponerlos en seguimiento y posteriormente confirmarlos.  
  [Referencia : status][Link N]  
  
  o.-**Log**: despues de hacer varias confirmaciones el comando ***git log*** nos permite ver el historial de todas las confirmaciones que se hayn echo al repositorio. Este comando nos da múltiples opciones, como ver las ultimas dos entradas de cada repositorio , mostrar los cambios en orden cronológico o mostrar las diferencias introducidas en cada confirmación como uno de sus usos mas útiles.  
  [Referencia: log][Link O]  
  
  p.-**Checkout**: se utiliza para crear nuevas ramas y moverse a tráves de ellas, al utilizar ***git checkout*** creas una rama y quedas de forma activa en ella (checkout) ó al saltar a otra rama mediante ***git checkout -b [nombrerama]*** se transforma en tu rama activa. Esto nos permite movernos por as distintas ramas modificando los archivos y actualizandolos para luego (si es requerido) utilizar ***merge*** para fusionar las ramas creadas.  
  [Referencia: checkout][Link P]  
  
  q.-**Rama**: como sabemos Git almacena los archivos como una serie de copias puntuales de los archivos completos, tal y como se encuentran en ese momento, al ir confirmando los cambios Git los ira guardando como objetos del arbol del repositorio, creando los datos pertinentes y un apuntador al árbol raíz. Por ende una rama es simplemente un apuntador móvil que apunta a cada confirmaciones que se vaya haciendo.
  La rama por defecto de Git es la rama master. Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación. En cada confirmación de cambios que realicemos, la rama irá avanzando automáticamente. Y la rama master apuntará siempre a la última confirmación realizada.  
  [Referencia: rama][Link Q]  
   
   r.-**Etiqueta**: las etiquetas son puntos especificos en el historial del repositorio, generalmente se utilizan para indicar cuando se haya lanzado alguna versión del proyecto. Se utilizan principalmente dos tipos de etiquetas, las ligeras que son como una rama que no cambia, es decir, un puntero a una confirmación específica, y las etiquetas anotadas, las cuales son almacenadas como objetos completos en la base de datos de Git. Tienen suma de comprobación; contienen el nombre del etiquetador, correo electrónico y fecha; tienen mensaje de etiquetado; y pueden estar firmadas y verificadas con GNU Privacy Guard (GPG). El uso de etiquetas nos permite saber cuando un proyecto posee una versión ya estable o terminada para ir actualizandola en el futuro.  
   [Referencia: etiqueta][Link R]  
   


  
  
  
  
  
  
  
  
  [Link A]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
  [Link B]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones  
  [Link C1]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos  
  [Link C2]:http://adrianmoya.com/2012/07/usando-git-de-manera-local/#sthash.XqrM6HwC.dpbs 
  [Link D]:https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms 
  [Link E]:http://gitready.com/beginner/2009/01/18/the-staging-area.html 
  [Link F]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio 
  [Link G]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio  
  [Link H]:https://www.git-tower.com/learn/git/commands/git-commit  
  [Link I]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git  
  [Link J]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos 
  [Link K]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos  
  [Link L]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos  
  [Link M]:https://www.atlassian.com/git/tutorials/using-branches/git-merge
  [Link N]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio  
  [Link O]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones  
  [Link P]:https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar 
  [Link Q]:https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F  
  [Link R]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas 
  
  
  