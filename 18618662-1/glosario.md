#Glosario Git

##Control de versiones (VC)
El control de versiones es un sistema que registra los cambios realizados sobre un archivo a lo largo del tiempo, con el fin de recuperar versiones especificas a medida que avanza el proyecto, permitiendo revertir archivos a su estado anterior, comparar cambios, etc.
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
##Control de versiones distribuido (DVC)
Es un tipo de sistema de control de versiones donde los clientes trabajando en el proyecto replican completamente el repositorio. De esta manera pueden restaurarse los datos utilizando cualquiera de los repositorios de los clientes en el caso que el servidor utilizado muera.
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
##Repositorio remoto y repositorio local
Un repositorio en git es un lugar de almacenamiento virtual para nuestro proyecto, en el cual podemos guardar las versiones de nuestro codigo y acceder a ellas cuando sea necesario. El repositorio local es un lugar de almacenamiento contenido dentro de nuestra computadora personal (no requiere conexion a internet), mientras que un repositorio remoto esta contenido en un servidor web, o simplemente en un lugar distinto al repositorio local.
https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes
##Copia de trabajo / Working Copy
Una copia de trabajo es una copia funcional de un repositorio, con los datos necesarios para trabajar.
https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository
##Area de preparacion / Staging area
Tambien llamado "index", esta es un area intermedia entre el directorio de trabajo y el repositorio, donde los commits pueden ser formateados y revisados antes de completarse.
https://book.git-scm.com/about/staging-area
##Preparar Cambios / Stage Changes
Preparar cambios es basicamente agregar los cambios a los archivos al staging area, para que git reconozca el cambio y lo incluya en el siguiente commit.
https://githowto.com/staging_changes
##Confirmar cambios /Commit changes
Confirmar cambios es utilizar el comando git commit para agregar los cambios hechos al repositorio local, necesitando un mensaje que resuma los cambios realizados para mejor organizacion.
https://www.atlassian.com/git/tutorials/saving-changes
##Commit
Es un comando de git con el cual se agregan cambios realizados en archivos al repositorio de trabajo, con un mensaje y un unico identificador hash.
https://help.github.com/articles/github-glossary/
##Clone
Es un comando de git el cual realiza una copia de todos los datos de un repositorio y los almacena en un nuevo directorio.
https://git-scm.com/docs/git-clone
##Pull
Es un comando de git el cual incorpora los cambios realizados en un repositorio remoto a la rama actual. Se compone por un git fetch y luego un git merge
https://git-scm.com/docs/git-pull
##Push
Es un comando de git con el cual se tranfieren los commits hechos en el repositorio local a un repositorio remoto (ramas remotas).
https://www.atlassian.com/git/tutorials/syncing
##Fetch
Es un comando de git que importa los commits realizados en un repositorio remoto al repositorio local, almacenandolos en ramas remotas, dando la oportunidad de revisar los cambios antes de integrarlos.
https://www.atlassian.com/git/tutorials/syncing
##Merge
Es un comando de git que incorpora los cambios realizados en una rama determinada a la rama actual. Puede resultar en un conflicto si en ambas ramas se ha modificado la misma seccion de codigo.
https://git-scm.com/docs/git-merge
##Status
Es un comando de git que muestra el estado del directorio de trabajo y el staging area, te deja ver que cambios fueron preparados y los archivos no monitoreados por git. Tambien contiene instrucciones para preparar archivos (staging).
https://www.atlassian.com/git/tutorials/inspecting-a-repository
##Log
Es un comando de git que muestra una lista de los commits realizados, el historial del proyecto, con lo cual se puede filtrar y buscar cambios especificos.
https://www.atlassian.com/git/tutorials/inspecting-a-repository
##Checkout
Es un comando de git que sirve para crear una nueva rama y posicionarse en ella. 
https://www.atlassian.com/git/tutorials/using-branches/git-checkout
##Rama / Branch
Una rama es una version paralela de un repositorio, la cual esta contenida dentro de este. Al tener varias ramas se puede trabajar en distintos aspectos del codigo sin modificar directamente la rama principal ("master"). Para guardar los cambios se puede ejecutar un merge entre la rama utilizada y la rama principal.
https://help.github.com/articles/github-glossary/
##Etiqueta / Tag
En git se pueden etiquetar commits especificos para identificarlos facilmente. Existen 2 tipos de tags: lightweight (ligero), el cual no cambia y es un puntero a un commit especifico, y annotated (anotado) que son guardados como objetos completos en la base de datos con informacion como el usuario que lo creo, etc.
https://git-scm.com/book/en/v2/Git-Basics-Tagging 