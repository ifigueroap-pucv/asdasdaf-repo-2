﻿





# Glosario

**- A. Control de versiones (VC):** _Combinación de de tecnologías y prácticas para controlar los cambios realizados en los ficheros de un proyecto en el código fuente, en la documentación y en las páginas web._
[Referencia](https://producingoss.com/es/vc.html)

**- B. Control de versiones distribuido :** _Permite  trabajar a desarrolladores trabajar en un proyecto común sin la obligación de trabajar en una mismas red._
[Referencia](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

**- C. Repositorio remoto y repositorio local:** _Los repositorios remotos son versiones de proyectos que se encuentran almacenados en la red o en Internet. En cambio un repositorio local guarda tus proyectos en tu ordenador._
[Referencia 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[Referencia 2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
		
**- D. Copia de trabajo / Working Copy:** _Es cuando se quiere copiar un repositorio Git desde otro ordenador._
[Referencia](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)
**- E. Área de Preparación / Staging Area:** _Es donde donde se almacena la información que irá en la próxima confirmación._
[Referencia](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**- F. Preparar cambios / Stage Changes :** _Cuando se quiere hacer cambios se confirman instantáneas de esos cambios en el repositorio_
[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**- G. Confirmar cambios / Commit Changes:** _Se confirman los cambios cada vez que el proyecto esté en un estado que se quiere conservar._
[Referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

**- H. Commit:** _Identifica los cambios realizados del trabajo_
[Referencia](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

**- I. Clone:** _Obtiene una copia de tu repositorio Git._
[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

**- J. Pull:** _Es la abrevación de **Fetch** seguido de **Merge**._
[Referencia](https://es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git)

**- K. Push:** _ sube los cambios realizados en tu trabajo a una rama de trabajo tuya y/o de tu equipo remota._
[Referencia](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

**- L. Fetch:** _Trae cambios realizados a otra **Branch**._
[Referencia](https://es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git)

**- M. Merge:** _En diferencia a Fetch, Merge trae los cambios a la **Branch** local._
[Referencia](https://es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git)

**- N. Status:** _Muestra el estado del árbol de trabajo._
[Referencia](https://git-scm.com/docs/git-status)

**- O. Log:** _Muestra los registros de confirmación._
[Referencia](https://git-scm.com/docs/git-log)

**- P. Checkout:** _Cambia ramas o restaura archivos del árbol de trabajo._
[Referencia](https://git-scm.com/docs/git-checkout)

**- Q. Rama / Branch:** _Crea una nueva rama, pero no salta hacia ella._
[Referencia](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-¿Qué-es-una-rama%3F)

**- R. Etiqueta / Tag:** _Sirve para apuntar lugares donde se ha realizado alguna versión._
[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)



