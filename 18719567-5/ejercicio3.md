﻿# Ejercicio 3

**A:** Cantidad y nombre de los branches
* Cantidad: 1
* Nombre de la rama: master

**B:** Cantidad y nombre de las etiquetas
* Cantidad: 0

**C:** Nombre, mensaje y código hash de los últimos 3 commits. 

**Commit 1:**
* Nombre:	sbaezamella	
* Mensaje:	Agrega glosario.md
* Código Hash: 24032a626edfcc4b60fdc108552fa83743831ba7

**Commit 2:**
* Nombre:	Geraldo
* Mensaje:	ingreso de commits
* Código Hash: 4aa23ad751d1f10106d49fa71de62f5d62b6ecc8

**Commit 3:**
* Nombre:	Nicolás Johnson
* Mensaje:	Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-2

* Código Hash: da69b727777f2037fe1016536b0ad958f2638c04


