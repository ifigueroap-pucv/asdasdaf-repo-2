﻿# Glosario

**A. Control de versiones (VC):**  El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**B. Control de versiones distribuido (DVC):** En un DVCS, los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**C. Repositorio remoto y repositorio local:** Los _repositorios remotos_ son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. El repositorio local corresponde a la carpeta de tu proyecto que esta en tu ordenador.
[Fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[Fuente 2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

**D. Copia de trabajo / Working Copy:** Copiar el repositorio de un ordenador remoto.
[Fuente](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)

**E. Área de preparación / Staging Area:** Corresponde a un area de almacenamiento intermedio usado para procesar datos.
[Fuente](https://en.wikipedia.org/wiki/Staging_(data))

**F. Preparar Cambios / Stage Changes:** Realizar el add a los archivos
[Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

**G. Confirmar Cambios / Commit Changes:** Se realizan los cambios de los archivos en el repositorio local.
[Fuente](https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push)

**H. Commit:** _Commit_ identifica los cambios hechos en dicho ambiente de trabajo. Si tu no haces un push de tus cambios, estos jamas se veran reflejados en tu repositorio remoto.
[Fuente](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

**I. Clone:** Crear una copia local del codigo remoto indicado por cierta direccion url.
[Fuente](https://stackoverflow.com/questions/2916849/what-do-these-words-mean-in-git-repository-fork-branch-clone-track)

**J. Pull:** Baja los cambios de la rama determinada y la actualiza contra tu repositorio local.
[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

**K. Push:** Corresponde a mandar archivos de tu repositorio local a uno remoto.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**L. Fetch:** Es como un pull pero sin hacer un merge (fusión) de los datos, dejándolos en otra rama.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**M. Merge:** Comando para fusionar ramas de tu proyecto.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Reorganizando-el-trabajo-realizado)

**N. Status:** Principal herramienta para determinar qué archivos están en qué estado es el comando status.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**O. Log:** Comando que muestra el historial de todas las modificaciones que se han hecho.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

**P. Checkout:** Comando para saltar de una rama a otra.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**Q. Rama / Branch:** Una rama Git es simplemente un apuntador móvil apuntando a una confirmacion de cambio.
[Fuente](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**R. Etiqueta / Tag:** Puntos específicos en la historia marcados como importantes.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)


