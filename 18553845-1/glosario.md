# Glosario GIT
## Nombre: Axel Jara Sánchez.

## Rut: 18553845-1.

## Asignatura: Ingeniería web.

A. Control de versiones (VC)
: El control de versiones hace referencia a la posibilidad de llevar el registro de las modificaciones que se han realizado en algún archivo. Este permite volver a alguna versión anterior o saber quién ha implementado ciertos cambios.

https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones


B. Control de versiones distribuido (DVC)
: Este funciona manteniendo la información del servidor o repositorio replicado en las diferentes máquinas que trabajan en él, es decir cada máquina cliente alberga la información del servidor de forma local. Muchos colaboradores pueden trabajar modificando el mismo repositorio.

https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

C. Repositorio remoto y Repositorio local
: Un repositorio local es en el cual se trabaja y se modifican los archivos, este tiene 3 partes (Inicial, Stage, Head). Inicial es donde se encuentran los archivos con los cuales trabajamos y se pueden editar, una vez modificados podemos pasarlos al Stage realizando un add del archivo quedando en esta etapa intermedia. Luego cuando estamos seguros del cambio se realiza un commit dejando el cambio en la etapa final head y modificado en nuestro repositorio pero aún no está en el repositorio remoto.

Los repositorios remotos son aquellos que almacenan archivos o proyectos en la red, a estos se les puede cargar y descargar archivos del mismo. De esta forma se comparten datos  a través de el con más personas.


https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/

http://rogerdudler.github.io/git-guide/index.es.html

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos


D. Copia de trabajo / Working Copy
: Es una copia de los archivos del sistema de control de versiones y se realiza con el comando checkout, sobre esta copia se trabaja y se modifican los archivos.

https://www.osmosislatina.com/cvs_info/wcopy.htm

E. Área de Preparación / Staging Area
: Esta es el área intermedia, una vez modificado un archivo y que se está seguro de los cambios se realiza el add dejando los archivos modificados en la stating area. Los cambios serán reflejados en el repositorio una vez que se confirme la acción por medio de un commit

https://www.youtube.com/watch?v=QGKTdL7GG24

F. Preparar Cambios / Stage Changes
: Acción de realizar el add de los archivos, pasando a la stating area.

https://www.youtube.com/watch?v=QGKTdL7GG24

G. Confirmar cambios / Commit Changes
: Se confirman los cambios y se realiza la modificación en el repositorio local.

https://githowto.com/commiting_changes

H. Commit
: Comando para confirmar los cambios y realizar la modificación en el repositorio, sale del stating area y pasa al head

https://www.youtube.com/watch?v=QGKTdL7GG24

I. Clone

: Comando para clonar un repositorio en tu maquina local, puede tomar mucho tiempo dependiente de la cantidad de archivos. Se copian todos los archivos que estén en el repositorio clonado

https://www.youtube.com/watch?v=QGKTdL7GG24

J. Pull
: Comando para traer la información y archivos que se encuentra en el repositorio común de trabajo, realiza un merge entre los datos de nuestro repositorio y el repositorio remoto.

https://www.youtube.com/watch?v=QGKTdL7GG24

K. Push
: Comando para enviar lo que se encuentra en nuestro repositorio al repositorio de remoto, esto actualiza para todos los archivos del repositorio común por lo cual para que otros colaboradores tengan los cambios n sus repositorios locales deben realizar un pull de este.

https://www.youtube.com/watch?v=QGKTdL7GG24

L. Fetch
: Comando similar al pull, trae los archivos del repositorio remoto pero los cambios los deja en otra rama, a la espera de un merge para fusionar estos. 

https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git


M. Merge
: Comando para fusionar ramas, puede presentar colisiones en caso que ambas ramas hayan modificado el mismo trozo de código.

https://www.youtube.com/watch?v=QGKTdL7GG24

N. Status
: Comando que muestra la situación en la que se encuentran los archivos, si estos han sido modificados, eliminados o si se encuentran en el stating area, una vez que se  realiza un commit el comando status no muestra nada.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

O. Log
: Comando que muestra el registro de los commit que se han realizado y sus comentarios.

https://www.youtube.com/watch?v=QGKTdL7GG24

P. checkout
: Comando que sirve para cambiar de rama en la cual se está trabajando, también puede ser utilizado para crear una rama con la opción -b.

https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar

Q. Rama / Branch
: Comando para crear una rama, se crea una rama a partir de la rama en la cual nos encontremos en ese momento. 

https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar

R. Etiqueta / Tag
: Comando para crear un alias a un commit y hacer referencia a este, por lo general se utilizan para establecer el lanzamiento de alguna versión. 

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
