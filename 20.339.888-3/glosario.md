##Glosario Taller N°0

1. **Control de versiones:** Básicamentee el cv es es una forma de guardar los cambios de un o unos archivo(s) a lo largo del tiempo, de esta forma se puede mantener un respaldo. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. **Control de versiones distribuido:** Lo especial de que seadistribuidoo es que lainformaciónn del trabajo se encuentra almacenado en distintos dispositivos, esto permite que se genera un trabajo colaborado, simultaneo y entrega una facilidad inmediata en el caso de tener que restaurarlo. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) 

3. **Repositorio remoto y Repositorio local:** Un repositorio remoto es unaversiónn del proyecto que se encuentra almacenado en la nube, estos se utilizan para colaborar con tu equipo de trabajo. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
El repositorio local funciona similar al remoto, sólo que en este caso los archivos se almacenan en el ordenador. [Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

4. **Copia de trabajo/Working copy:** Una copia de trabajo se define como una copia exacta del directorio especificado, esto necesario para trabajar dado que de esta forma cualquier cambiotambiénn se hace en cualquier documento. [Fuente](https://www.osmosislatina.com/cvs_info/wcopy.htm)

5. **Área depreparaciónn/Staging area:** Se define como un archivo contenido en tu directorio de trabajo, el cual almacena lainformaciónn local que estas trabajando, para luego ser almacenadas de forma permanente en el directorio git. [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git) 

6. **Preparar cambios/Stage changes:** Esta etapa se caracteriza por, dejar los documentos a punto de ser subidos en el directorio con laoperaciónn add, este proceso se cancela si tras haber realizado git add uno modifica el archivo. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) 

7. **Confirmar cambios / Commit Changes:** Confirmar,finalmentee significa actualizar y subir sólo los archivos que has preparado anteriormente, a tu repositorio, para hacer esto efectivo se utiliza l comando commit.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) 

8. **Commit:** Este comando almacena el contenido actual de tus cambios preparados, confirmandolos junto a un mensaje de registro donde el usuario describe sus cambios [Fuente](https://git-scm.com/docs/git-commit)

9. **Clone:** Lo que hace este comando es clonar un repositorio en un directorioreciénn creado, genera y chequea ramas se seguimiento que se separa de la rama activa del repositorio clonado [Fuente](https://git-scm.com/docs/git-clone)

10. **Pull:** Git pull lo que hace es incorporar cambios del repositorio remoto en la rama actual de trabajo, esto es una abreviatura de gitfetch seguido de git merge FETCH_HEAD. [Fuente](https://git-scm.com/docs/git-pull) 

11. **Push:** En si la funcionalidad de git push es añadir un archivo que desees compartir desde tu repositorio local a un repositorio remoto, donde tras el comando uno especifica desde donde quieres enviarlo(master) y su destino(origin). Por ejemplo: $git push origin master [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 


12. **Fetch:** Principalmete lo que hace este comando es recuperar todos los datos del proyecto remoto que aun no tengasdejándolaa en tu repositorio local, aunque esto no lo une a tu trabajo actual ni lo modifica. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 


13. **Merge:** Lo que hace es fusionar ciertos cambios de commits especificos, de una rama a la rama actual de trabajo, se puede usar para incorporar cambios desde otro repositorio y/o fusionar cambios de una rama a otra. [Fuente](https://git-scm.com/docs/git-merge)

14. **Status:** Muestra las diferencias si es que existen, del index al head, entre el desde el directorio de trabajo y el index. En pocas palabras muestra el estado de lo que estamos haciendo y particularmente si es que hay algo mal en ello. [Fuente](https://git-scm.com/docs/git-status)

15. **Log:** Funciona para ver unhistóricoo de las modificaciones que se han llevado a cabo en el repositorio, donde muestra el autor, la fecha el mensaje y sucódigoo de commit. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

16. **Checkout:** Lo que hace particularmente es salir de tu rama de trabajo generando una copia exacta que mantiene los mismos atributos que la original, de esta forma puedes trabajar en particular en este espacio, tras esto con el mismo comando puedes hacer para volver a tu rama de trabajo original. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar) 
17. **Branch:** Se le denomina a las ramas de trabajo que uno posee en un repositorio, el comando git branch es generar una nueva rama que apunte a la misma que estabas inicialmente. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. **Tag:** Lo mas utilizado para este comando es para poder marcar puntos importantes dentro de tuhistoriall, tales como las versiones (1.0 - 1.0.1 - etc) ademas de poder buscar tags especificas, ver sus datos, etc.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)  