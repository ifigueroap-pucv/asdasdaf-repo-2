﻿# Glosario Git
**_a) Control de versiones (VC) :_** El control de versiones es un sistema que registra los cambios sobre un archivo o conjunto de archivos a lo largo del tiempo.
	[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**_b) Control de versiones distribuido (DVC):_** Es un sistema que permite a muchos desarrolladores de software trabajar e un proyecto común sin necesidad de compartir una misma red, esto soluciona el problema de los sistemas de control de versiones centralizados ya que los desarrolladores replican completamente el repositorio. 
[Fuente1](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)
[Fuente2](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**_c) Repositorio remoto y Repositorio local:_** Los repositorios remotos son versiones del proyecto que se encuentran alojados en Internet o en algún punto de la red. En cambio un repositorio local es aquella versión que es almacenada en el ordenar y es a la que se le van aplicando cambios.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**_d) Copia de trabajo/Working Copy:_** Es una copia que has destinado a tu area de trabajo, es en lo que estas trabajando en el momento.
[Fuente](https://stackoverflow.com/questions/581220/what-is-a-working-copy-and-what-does-switching-do-for-me-in-tortoise-svn) 

**_e) Área de Preparación/Staging Area:_** Es el sistema que permanece entre las fuentes de datos y el data warehouse, facilita la extracción de datos desde las fuentes de origen, realiza la limpieza de datos (cleansing), entre otros.
[Fuente](http://josepcurto.com/2007/10/15/que-es-una-staging-area/)

**_f) Preparar Cambios/Stage Changes:_**  Consiste en notificar a Git que hay archivos que han sido modificados, y que estos serán incluidos en la próxima confirmación
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**_g) Confirmar Cambios/Commit Changes:_**  Es la confirmación de los cambios que se efectuaran al hacer un commit, estos cambios se mostraran en pantalla al utilizar el comando git status.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**_h) Commit:_** Sirve para confirmar los cambios realizados, es recomendable incluir un mensaje preciso sobre los cambios.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**_i) Clone:_** Comando que permite obtener una copia de un repositorio existente y dejarla en un repositorio local.
[Fuente](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)

**_j) Pull:_** Comando que baja los cambios de una rama determinada y la actualiza en el repositorio local, en otras palabras es la combinación de fecth y merge.
[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

**_k) Push:_** Comando que permite subir los datos ubicados en el repositorio local, al repositorio remoto.
[Fuente](https://git-scm.com/docs/git-push)

**_l) Fetch:_** Comando que baja los cambios de una rama para dejarlos en una rama equivalente en tu repositorio, donde puedes hacer consultas de los cambios, para luego hacer un merge con la rama local.
[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

**_m) Merge:_** Comando que permite la unión o mejor dicho fusión de dos ramas (o branch).
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

**_n) Status:_** Comando que muestra el estado de trabajo actual, y las diferencias entre los caminos, además de la posición actual del puntero HEAD.
[Fuente](https://git-scm.com/docs/git-status/1.8.1)

**_o) Log:_** Comando que permite mirar atrás para revisar que modificaciones se han llevado a cabo en el repositorio.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)


**_p) Checkout:_** Comando que sirve para situar un apuntador especial en la rama en la que se quiere trabajar.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**_q) Rama/Branch:_** Una rama es simplemente un apuntador móvil apuntando a la distintas confirmaciones que se realizaron, en palabras mas simples distintas versiones del proyecto.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**_r) Etiqueta/Tag:_** Funcionalidad que permite etiquetar puntos especificos en la historia del proyecto denominados importantes.
[Fuentes](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)




