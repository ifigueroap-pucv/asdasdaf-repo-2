﻿# Glosario
## Control de versiones (VC) 
Sistema que registra los cambios que se van realizando en el tiempo sobre un archivo o un conjunto de estos, con el fin de recuperar versiones específicas en el futuro.
[Referencia bibliográfica https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones]
## Control de versiones distribuido (DVC)
Sin necesidad de compartir una misma red, el control de versiones distribuido, permite a varios desarrolladores de software trabajar al mismo tiempo en un proyecto común, almacenando las revisiones en un DVC.
[Referencia bibliográfica https://es.wikipedia.org/wiki/Control_de_versiones_distribuido]
## Repositorio remoto y Repositorio local 
Son versiones de un proyecto que se ubican alojados en Internet o en algún lugar de la red. Se pueden tener varios, y según los permisos que se tengan pueden ser sólo de lectura, o de lectura/escritura. Colaborar con otros implica poder administrar estos repositorios remotos, con comandos como pull (recibir) y push (enviar) de ellos a medida que se necesite.
[Referencia: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos]
## Copia de trabajo / ​Working Copy 
Las copias de trabajos sirve como copias de seguridad. Así, si un servidor muere, se puede hacer una restauración, en caso de necesitarlo.
[Referencia: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones]
## Área de Preparación / ​Staging Area 
Es una de las tres secciones principales de un proyecto de Git, llamada al español, área de preparación.  Es un archivo sencillo, generalmente contenido en un directorio de Git, que almacena información sobre lo que irá en una próxima confirmación. Algunas veces se le denomina índice, pero se está transformando en estándar el denominarse a ella como el área de preparación. 
[Referencia: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git]
## Preparar Cambios / ​Stage Changes 
Consiste en la notificación de Git sobre la modificación de archivos que deben ser incluídos en la próxima confirmación.
[Referencia: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio]
## Confirmar cambios / ​Commit Changes
Consiste en la confirmación de las modificaciones que han sido efectuadas al hacer un _commit_, cuyos cambios se muestran por pantalla al usar el comando _git status_.
[Referencia: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio]
 
## Commit
  Como sustantivo, Commit se refiere a un hito en el historial de Git. Todo el historial de un proyecto está representado por una colección de *"commits"* interrelacionados. _Commit_ se entiende también como "Versión" o "Corrección". 
  Como verbo, _Commit_ se refiere a la acción de almacenar una nueva imágen del estado del proyecto en el historial de Git, creando una nueva versión representando el estado actual del índice, y avanzando el HEAD apuntando a la nueva versión.
  [Referencia: https://git-scm.com/docs/gitglossary]
## clone 
Es la acción de copiar un repositorio Git ya existente hacia un nuevo directorio creado.
[Referencia: https://www.git-scm.com/docs/git-clone]
## pull 
Incorpora cambios desde un repositorio remoto hacia la rama actual.  La acción _git pull_ corresponde a un _git fetch_ seguido por un _git_ _merge_ _FETCH_HEAD_. 
Más precisamente, _git pull_ corre un _git fetch_ con unos parámetros dados y llama a _git merge_ para incorporar las cabezas de rama recuperadas hacia la rama actual.
[Referencia: https://git-scm.com/docs/git-pull]
## push 
Hacer push a una rama significa obtener la referencia de la cabeza de una rama desde un repositorio remoto, y descubrir si es un ancestro directo de la referencia de la cabeza de la rama. Si es el caso, pone todo los objetos obtenibles usando la referencia de la cabeza local, los cuales falten en el repositorio remoto, hacia una base de datos de objetos, y actualizando la referencia de la cabeza remota. Si la cabeza remota NO es un ancestro de la cabeza local, la acción push o mandar falla.
[Referencia: https://git-scm.com/docs/gitglossary]
## fetch 
git-fetch lo que hace es descargar objetos y referencias de otro repositorio Git en una carpeta oculta llamada "origin/master". Se puede hacer desde un repositorio particular o desde varios repositorios a la vez utilizando < group >.  
[Referencia: https://git-scm.com/docs/git-fetch]
## merge 
La acción de traer contenidos de otra rama (posiblemente desde un repositorio externo) hacia la rama actual o activa. 
[Referencia: https://git-scm.com/docs/gitglossary]
## status 
Es una herramienta que tiene como fin determinar cuál es el estado de los archivos encontrados al interior de un repositorio.
[Referencia: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio]
## log 
Un registro que muestra las modificaciones que se han llevado a cabo en el historial de confirmaciones.
[Referencia: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones ]
## checkout 
Es la acción de actualizar todo o parte del _árbol de trabajo_ desde la base de datos de objetos, actualizando el índice y el HEAD si es que todo el árbol de trabajo ha sido apuntado hacia una nueva rama.
[Referencia: https://git-scm.com/docs/gitglossary]
## Rama / Branch 
Una rama es una línea activa de desarrollo. La versión más reciente en una rama es denominada como la punta de la rama en cuestión. La punta de la rama es referenciada por una cabeza de rama, la cual avanza a medida que se hacen desarrollos adicionales sobre la rama. Un repositorio Git único puede rastrear un número arbitrario de ramas, pero tu árbol de trabajo está asociado con sólo uno de ellos, con el HEAD apuntando hacia esa rama.
[Referencia: https://git-scm.com/docs/gitglossary]
## Etiqueta / Tag
Es una marca o clasificación en el historial de nuestro repositorio, que permiten marcar nuestro proyecto cuando se lance una versión nuevo. 
Existen de dos tipos: los ligeros y los anotados. Los primeros serían algo así como un enlace o puntero a un _commit_ en particular o rama que no cambia, y los _anotados_ serían una copia completa del estado del repositorio en cuestión.
[Referencia: https://blog.unelink.es/wiki/como-usar-tags-en-git ]




