#Glosario **Usando Git for Windows**

##a.Control de Versiones
Es la gestión de de los cambios que se realizan en los distintos elementos o en las configuraciones de algún producto, donde la versión es el estado en el que se encuentra este.
Fuente: *https://es.wikipedia.org/wiki/Control_de_versiones*

##b.Control de Versiones Distribuido
Es identico al control de versiones pero este permite que multiples desarrolladores trabajen sobre un mismo software sin tener la necesidad de estar en la misma red. Permite que todos los clientes tengan la version actual en todos sus computadores.
Fuente: *https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones*

##c.Repositorio Remoto y Repositorio Local
Los __repositorios remotos__ son versiones de un proyecto que se alojan en la Internet o en algun punto de la red. Pueden ser de lectura, lectura/escritura, eso dependera de los permisos con los que se disponga. Permiten la colaboracion con otras personas y para poder gestionar se deben mandar(push) o recibir(pull).
El __repositorio local__ funciona de la misma forma que el remoto, sin embargo, este de aloja en algun directorio dentro de tu computadora.
Fuentes: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos* & *https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/*

##d.Copia de Trabajo / *Working Copy*
La copia de trabajo es el uso de git para copiar un repositorio de la red para poder tener una copia de seguridad local en caso de cualquier acontecimiento.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git*

##e. Área de Preparación / *Staging Area*
Es un archivo sencillo que cumple con la funcion de almacenar la informacion que ira a la proxima confirmacion, suele estar en el el directorio local de Git.  
Fuente: *https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git*

##f. Preparar Cambios / *Stage Changes*
Es cuando se notifica a Git sobre cambios que fueron realizados en los archivos para que este los incluya en la proxima confirmacion.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio*

##g. Confirmar Cambios / *Commit Changes*
notifica a git para confirmar los cambios que se hicieron y de los cuales se dio aviso en _Preparar Cambios_ y asi guardarlos en la nueva version del proyecto. Aquellos cambios que no fueron preparados no seran confirmados y quedaran en el disco como archivos locales solamente.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio*

##h. _Commit_
Es el comando utilizado para poder enviar la notificacion a Git sobre la confirmacion de los cambios.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio*

##i. _clone_
Es el comando que utiliza para poder clonar un repositorio ya existente, ya sea en la web o que sea uno local.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git*

##j. _pull_
Cuando existe una rama configurada para seguir otra remota, pull ayuda a recuperar y unir la rama remota con la rama actual.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos*

##k. _push_
Es el comando que se usa para poder enviar los archivos o el proyecto a un repositorio remoto. 
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos*

##l. _fetch_
Este comando se usa para recuperar todos los archivos de un repositorio remoto, es decir, es para recibir archivos.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos*

##m. _merge_
Este comando fusiona la rama actual con la rama maestra, hay que tener cuidado al usarla ya que los cambios perduran asi que deben hacerce las pruebas necesarias antes de usarlo.
Fuente: *https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-basicos-para-ramificar-y-fusionar*

##n. _status_
Este comando entrega la informacion de cual es el estado de los archivos, asi poder comprobar si los otros comandos hicieron lo que deberian.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio*

##o. _log_
Es un comando que sirve para ver el historial de confirmaciones, es decir, es para mirar atras en todo lo que se ha modificado sobre le proyecto. 
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el histórico-de-confirmaciones

##p. _checkout_
Es un comando utilizado para saltar de una rama a otra, deber ser sucedido de la opcion de rama a la que se desea saltar.
Fuente: *https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-basicos-para-ramificar-y-fusionar*

##q. Rama / Branch
La rama git es un puntero o apuntador que señala a las distintas confirmaciones que se han ido presentando durante el proyecto, dentro de ellos hay uno que es la rama master que vendria a ser la primera modificacion o confirmacion del proyecto. 
Se utiliza la funcion *branch*.
Fuente: *https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F*

##r. Etiqueta / Tag
Sirve para poder etiquetar los puntos especificos en la historia, se usa para dejar marcas generalmente en la version, se usa el comando *tag*.
Fuente: *https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas*
