﻿
Ingeniería WEB  
\- Glosario

  1.- Control de Versiones (VC) : Un sistema de control de versiones es un sistema el cual te permite ir almacenando el progreso que se lleva al momento de llevar a cabo un proyecto.

Todo esto a modo de respaldo ya que en el caso que ocurra un fallo durante el desarrollo de un programa , una version anterior se encontrará disponible sin fallos y funcional.

  

LINK : [https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/](https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/)

  

2.- Control de Versiones Distribuido (DVC): A diferencia del control de Versiones común, el DVC no posee un servidor central del cual se obtienen las versiones, en este caso cada programador tiene una copia del programa en sus dispositivos. Como beneficio principal es que al llevar a cabo desarrollos de gran envergadura es mucho más fácil encontrar diferencia entre los archivos.

  

LINK:[http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)

  

3.- Repositorio Local: Como lo dice su nombre un repositorio local hace referencia la implementación de un espacio en tu computador en donde se pueden almacenar las distintas versiones de tu programa o desarrollo.

  

LINK : [https://definicion.de/repositorio/](https://definicion.de/repositorio/)

  

4.- Repositorio Remoto: Al igual que el repositorio local , un repositorio remoto es aquel que se encuentra en un servidor y del cual todos los participantes del desarrollo del SW pueden acceder a los distintos archivos del programa.

  

LINK : [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

  

5.- Copia de trabajo: una copia de trabajo es una forma de mantener siempre una versión del programa la cual sea completamente funcional y sin fallos, ya que , en caso de que ocurra algo al momento de modificar el código , esta se encontrará disponible para retomar el trabajo.

LINK : [https://neliosoftware.com/es/blog/copias-de-seguridad-con-git/](https://neliosoftware.com/es/blog/copias-de-seguridad-con-git/)

  6.- Área de Preparación:  es un archivo que contiene los cambios preparados pero no confirmados.
LINK : https://git-scm.com/book/es/v2

7.- Preparar Cambios: prepara los cambios hechos añadiéndolos al área de preparación
LINK : https://git-scm.com/book/es/v2

8.- Confirmar Cambios: confirma los cambios hechos que están en el área de preparación, guardándolos en el repositorio local. 
 LINK : https://git-scm.com/book/es/v2

9.- Commit: El comando commit en GIT es el encargado de modificar la cabecera de forma local, por otra parte este comando no realiza cambios en el repositorio remoto.
LINK : https://git-scm.com/book/es/v2
  

LINK: [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  

10.- Clone : Este comando se encarga de revisar los repositorios mediante una direccion ya sea de internet o de archivos.

  

LINK : [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  

11.- PULL:Comando que tiene como función principal fusionar todos los cambios que se hayan hecho en el repositorio local.

  

LINK: [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  

12.- PUSH: Este es uno de los comandos más básicos. Un simple push envía los cambios que se han hecho en la rama principal de los repertorios remotos que están asociados con el directorio que está trabajando.

  

LINK : [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  

13.- Fetch : Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que está trabajando

  

LINK: [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  

14.- Merge: Este comando se usa para fusionar una rama con otra rama activa:

  

LINK:[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  

15.- Status : Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que están por ser añadidos o comprometidos.

  

LINK :[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  

16.- LOG : Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles

  

LINK :[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  
  
  
  

17.- CheckOut : El comando checkout se puede usar para crear ramas o cambiar entre ellas.

  

LINK :[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  

18.- BRANCH : Este comando se usa para listar, crear o borrar ramas.

  

LINK : [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

  
  

19.- Etiqueta / TAG : Etiquetar se usa para marcar commits específicos con asas simples.

  

LINK : [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)


