# Glosario

* a. Control de versiones (VC): El sistema de control de versiones permite al desarrollador poder guardar distintas versiones de su proyecto, esto permite en primer lugar tener el software ordenado, ya que as� se sabe que versi�n es m�s nueva y vieja, adem�s permite al desarrollador en caso de necesitar volver a trabajar en una versi�n anterior hacerlo. 
Fuente: [Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

* b. Control de versiones distribuido (DVC): El control de versiones distribuido es una forma de mantener un control de versiones, en el los desarrolladores estar�n conectados a un servidor, donde se almacenaran las versiones, pero la ventaja es que adem�s de que las versiones quedan en el servidor, estas tambi�n son copiadas totalmente en el ordenador, lo cual produce que los desarrolladores en caso de estar ca�do el servidor tambi�n puedan conectarse entre ellos y as� no perder la informaci�n o las versiones.
Fuente: [Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

* c. Repositorio remoto y Repositorio local: El repositorio local es aquel en el cual el desarrollador almacena sus proyectos dentro de la misma computadora. En cambio el repositorio remoto es un repositorio el cual esta online, en donde los desarrolladores se pueden conectar y dependiendo de los permisos que tengan pueden ver/escribir dentro del repositorio.
Fuente: [Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

* d. Copia de trabajo / Working Copy: Una copia de trabajo, se hace cuando se clona un repositorio y posee todo el trabajo dentro del repositorio.
Fuente: [Git - Obteniendo un repositorio Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* e. �rea de Preparaci�n / Staging Area: El area de preparaci�n es un archivo en git, donde se almacena informaci�n de lo que se har� en la pr�xima confirmaci�n.
Fuente: [Git - Empezando Fundamentos de Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

* f. Preparar Cambios / Stage Changes: Cuando se modifica un archivo, git detecta que el archivo fue modificado, pero para preparar cambios el desarrollador debe usar el comando add, lo cual preparara el archivo para ser a�adido una vez se confirmen los cambios.
Fuente: [Fundamentos de Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

* g. Confirmar cambios / Commit Changes: En la fase de confirmar cambios, se utiliza el comando _commit_ el cual subira tus cambios, siempre y cuando estos est�n preparados.
Fuente: [Fundamentos de Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

* h. _Commit_: _Commit_ es el comando de git que permite guardar los cambios hechos en el proyecto.
Fuente: [Git - Commit](https://git-scm.com/docs/git-commit)

* i. _clone_: _clone_ es el comando de git que clona todos los archivos de un repositorio para poder trabajar en �l, _clone_ copia todas las versiones de la historia del proyecto.
Fuente: [Fundamentos de Git - Obteniendo un repositorio Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* j. _pull_: _pull_ es el comando de git que permite obtener el c�digo de una rama remota, con el codigo de la rama en que nosotros estamos trabajando.
Fuente: [Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

* k. _push_: _push_ Es el comando de Git que permite enviar nuestro proyecto a un repositorio remoto y as� poder trabajar con �l de forma compartida.
Fuente: [Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

* l. _fetch_: _fetch_ permite actualizar la informaci�n de un repositorio remoto, pero este comando no la une autom�ticamente con tu trabajo, solo lo deja en tu repositorio local (para que se una autom�ticamente con nuestro trabajo tendr�amos que utilizar _pull_).
Fuente: [Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

* m. _merge_: _merge_ permite fusionar ramas, es decir si estamos trabajando en una rama y la queremos unir a la rama principal se utilizara este comando.
Fuente: [Git - Procedimientos b�sicos para ramificar y fusionar](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

* n. _status_: _status_ permite ver cual es el estado del trabajo actual, es decir muestra en que archivos hemos trabajado, cuales ya est�n preparados, cuales podr�amos preparar utilizando _add_ y permite saber en qu� rama estamos trabajando.
Fuente: [Git - status](https://git-scm.com/docs/git-status)

* o. _log_: _log_ permite ver todos los cambios o _commits_ hechos en el repositorio.
Fuente: [Git - Viendo el hist�rico de confirmaciones](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

* p. _checkout_: _checkout_ permite intercambiar entre ramas y adem�s permite crear una nueva y enseguida entrar a esta.
Fuente: [Git - Procedimientos b�sicos para ramificar y fusionar](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

* q. Rama / Branch: Una rama es una especie de puntero, el cual se sit�a en una de las modificaciones del proyecto, la utilidad de esta es que el desarrollador puede crear una rama independiente del proyecto la cual le permite trabajar en ella sin editar nada del proyecto el cual esta en la rama principal.
Fuente: [Git - �Qu� es una rama?](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

* r. etiqueta / tag: Las etiquetas permiten darle nombre a un _commit_ del proyecto, lo cual es muy �til si en un futuro necesitamos ver este, as� se le pueden dar nombre a las versiones como por ej: "V2.2".
*Fuente: [Git - Creando etiquetas](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
