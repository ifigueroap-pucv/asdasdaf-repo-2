#GLOSARIO DE TERMINOS

##Control de Versiones (VC):
El **control de versiones** es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones especificas mas adelante.  
[Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

##Control de Versiones distribuido (DVC):
El **control de versiones distribuido** hace que desarrolladores de software puedan trabajar en un proyecto comun sin necesidad de compartir una misma red. Estas versiones se almacenan en un sistema de control de versiones distribuido (por ejemplo Git). En este, no solo pueden descargar la ultima instantanea de los archivos, sino que replican completamente el repositorio. Asi, si un servidor muere, y estos sistemas estaban colaborando a traves de el, cualquiera de los repositorios de los usuarios puede copiarse en el servidor para restaurarlo.  
[Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)     
[Control de versiones distribuido](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

##Repositorio remoto y Repositorio local:
Los **repositorios remotos** son versiones de tu proyecto que estan hospedadas en Internet en cualquier otra red (por ejemplo, los repositorios remotos de Git).  Mientras que los **repositorios locales** son versiones que se encuentran en el computador.  
[Git y GitHub - Trabajando con repositorios locales](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales)  
[Git - Trabajar con remotos](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)

##Copia de trabajo / Working Copy:
Una **copia de trabajo** es la informacion descargada desde un repositorio remoto al repositorio local al efectuar la clonacion. La descarga corresponde a la ultima version.  
[Git - Obteniendo un repositorio Git](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)

##Area de preparacion / Starting Area:
El area de preparacion es un sencillo archivo, generalmente contenido en tu directorio de Git, que almacena informacion acerca de lo que va a ir en tu proxima confirmacion.   
[Git - Fundamentos de Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

##Preparar Cambios / Stage Changes:
**Preparar Cambios** es el paso en que los archivos se agregan con el comando **add** en el repositorio local, alistandose para agregarse al repositorio remoto.    
[6. Starting the changes](https://githowto.com/staging_changes)

##Confirmar Cambios / Commit Changes:
**Confirmar Cambios** es el paso en que una vez listos los archivos a agregar o modificar (en que se aplico **add**), confirmen los cambios al aplicar el comando **commit**.  
[Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

##commit:
El comando **commit** es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto.  
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##clone:
El comando **clone** se usa para crear una copia local del repositorio. 
[git - la guia sencilla](http://rogerdudler.github.io/git-guide/index.es.html)

##pull:
El comando **pull** es usado para poder fusionar todos los cambios que se han hecho en el repositorio local.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##push:
El comando **push** es uno de los comandos mas basicos. Un simple push envia los cambios que se han hecho en la rama principal de los repertorios remotos que estan asociados con el directorio que esta trabajando.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##fetch:
El comando **fetch** le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que esta trabajando.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##merge:
El comando **merge** se usa para fusionar una rama con otra rama activa.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##status:
El comando **status** muestra la lista de los archivos que se han cambiado junto con los archivos que estan por ser agregados o comprometidos.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##log:
El comando **log** al ejecutarlo, muestra una lista de commits en una rama junto con todos los detalles. 
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##checkout:
El comando **checkout** se puede usar para crear ramas o cambiar entre ellas.
[Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

##Rama / Branch:
Las **ramas** son utilizadas para desarrollar funcionalidades aisladas unas de otras
[git - la guia sencilla](http://rogerdudler.github.io/git-guide/index.es.html)

##Etiqueta / Tag:
 Generalmente se agregan **etiquetas** para marcar puntos donde se ha lanzado alguna version
[Git - Creando Etiquetas](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)