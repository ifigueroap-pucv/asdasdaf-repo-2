# Glosario
-  **Control de Versiones (VC)**
Sistema que permite tener un registro de todas las modificaciones que haya sufrido un archivo a lo largo del tiempo, lo que permite la recuperación de versiones anteriores, comparar cambios, ver quién realizó una modificación por última vez, etc. 
(*Fuente: [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)*)
- **Control de Versiones Distribuido (DVC)**
Sistema que permite replicar todo el repositorio, lo que a futuro puede servir como un respaldo en caso de pérdida de datos o mal funcionamiento del servidor.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)*)

- **Repositorio Remoto y Repositorio Local**
El repositorio remoto consiste en versiones de archivos que se encuentran guardados en la red. Estos repositorios pueden se de lectura o lectura/escritura. Mientras que el repositorio local son archivos que están almacenados en el disco duro del computador.
(*Fuente: [Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)*)
- **Copia de Trabajo / _Working Copy_**
Se trata de la última versión que se descarga luego de clonar un repositorio remoto.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)*)
- **Área de Preparación / _Staging Area_**
Un proyecto de Git tiene 3 secciones principales, una de ellas es el Área de preparación, esta es un archivo que contiene información de lo que está próximo a confirmarse.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)*)
- **Preparar Cambios / _Stage Changes_**
Consiste en usar *git add* a archivos modificados para rastrearlos y así puedan ser confirmados más adelante.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)*)
- **Confirmar Cambios / _Commit Changes_**
Se toman los archivos del área de preparación y se almacena una copia en el directorio.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)*)
- **_commit_**
Confirma los cambios que se hayan realizado a un archivo.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)*)
- **_clone_**
Comando que se utiliza para copiar un repositorio existente en un servidor.
(Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git))
- **_pull_**
Se recuperan y combinan automáticamente la rama remota con la rama actual.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)*)
- **_push_**
Se envían todos los *commits* que se hayan realizado al servidor.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)*)
- **_fetch_**
Recupera todos los archivos del repositorio remoto que faltan (desde la última vez que se clonó o se usó *fetch*). Además de tener referencias a las ramas remotas.
(*Fuente: [Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)*)
- **_merge_**
Corresponde a la fusión de ramas, es decir, se recopilan los cambios confirmados en una rama y se aplican sobre otra.
(*Fuente: [Git](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)*)
- **_status_**
Muestra el estado actual de los archivos, esto es, si han sido modificados o están bajo seguimiento.
(*Fuente: [Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)*)
- **_log_**
Muestra las últimas confirmaciones realizadas en el repositorio, ordenadas de forma cronológica.
(*Fuente: [Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)*)
- **_checkout_**
Comando que se usa para cambiar de rama. Básicamente es mover el apuntador HEAD a la rama deseada.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F#_git_branching)*)
- **Rama / _Branch_**
Corresponde a un apuntador a confirmaciones. La creación de ramas es beneficioso para trabajar los archivos de manera diferente. 
(*Fuente: [Git](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F#_git_branching)*)
- **Etiqueta / _Tag_**
Un apuntador a un *commit* específico. Utilizado frecuentemente para marcar un punto importante en el proyecto, como una nueva versión.
(*Fuente: [Git](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)*)