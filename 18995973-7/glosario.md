﻿## Glosario:


##### *a) Control de versiones(VC)*: 
 Es un sistema que registra los cambios que se realizaron sobre un archivo o un conjunto de estos, donde posteriormente estos pueden recuperarse mas adelante.

fuente: *(https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)*

#####  b) Control de versiones distribuido (DVC):
Es donde el cliente descarga la ultima versión de los archivos,y se copia completamente el repositorio.

fuente:*(https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)*

#####  c) Repositorio remoto y repositorio local: 

*Repositorio remoto*: Versiones de mi proyecto que estan alojadas en internet o en alguna otra red.
*Repositorio local*: Carpetas de mi proyecto que estan alojadas en mi computador.

fuente:*(https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)*

##### d) Copia de trabajo /working copy : 
Es una copia de trabajo de CVS que es utilizado para el desarrollo y control de versiones de software.

fuente:*(https://translate.google.cl/translate?hl=es&sl=en&u=https://workingcopyapp.com/&prev=search)*

##### e) Área de preparación/staging area:
 Es el área en el cual se guardan los cambios realizados en el archivo punto git.
 
fuente: *(https://translate.google.cl/translate?hl=es&sl=en&u=https://git-scm.com/book/en/v2/Getting-Started-Git-Basics&prev=search)*

##### f ) Preparar cambios/stages changes : 
Modo donde se pueden añadir, eliminar y modificar archivos antes del commit.

fuente: *(https://translate.google.cl/translate?hl=es&sl=en&u=https://githowto.com/staging_changes&prev=search)*

##### g) Confirmar cambios/commit changes :
Significa que los cambios realizados se guardarán en el repositorio.

fuente:*(https://networkfaculty.com/es/video/detail/2601-git---commit---confirmar-cambios)*


##### h) Commit :
 Se utiliza para registrar cambios en el repositorio.

fuente:*(https://translate.google.cl/translate?hl=es&sl=en&u=https://git-scm.com/docs/git-commit&prev=search)*

##### i) Clone:
Comando que permite clonar todo lo del repositorio remoto al local.

fuente:*(https://git-scm.com/docs/git-clone)*

##### j) Pull:
Descargar los cambios que no existían del repositorio remoto al local.

fuente:(https://translate.google.cl/translate?hl=es&sl=en&u=https://git-scm.com/docs/git-pull&prev=search)

##### k) Push: 
Se utiliza para transferir nuevas publicaciones desde el repositorio local al remoto.

fuente:*(https://translate.google.cl/translate?hl=es&sl=en&u=https://www.atlassian.com/git/tutorials/syncing&prev=search)*


##### l) Fetch: 
Baja los cambios de una sub carpeta determinada y los pone en una rama espejo, en la cual tu puedes tener acceso a los cambios realizados.

fuente:*(https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)*

##### m) Merge: 
Integrar commit de una rama de git a otra.

fuente :*(https://git-scm.com/docs/git-merge)*

##### n) Status:
Muestra los estados de los archivos en nuestra direccion de trabajo.Archivos modificados,sin seguimiento, no confirmados etc...

fuente:*(https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Seguimiento-B%C3%A1sico)*

##### o) Log: 
Se utiliza para mostrar los registros del commit.

fuente:*(https://translate.google.cl/translate?hl=es&sl=en&u=https://git-scm.com/docs/git-log&prev=search)*

##### p) Checkout:
 Se utiliza para cambiar de ramas, o restaurar archivos del arbol de trabajo.

fuente:*(https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)*

##### q) Rama/Branch: 
Una rama es un apuntador móvil que apunta a una confirmación de cambios. El comando git branch se encarga de crear, listar o borrar ramas.

fuente:*(https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)*


##### r) Etiqueta/tag : 
Se puede utilizar para marcar un punto especifico en la historia de un repositorio como importante. También pueden ser útiles para marcar hitos de progreso.

fuente:*(https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git)*

