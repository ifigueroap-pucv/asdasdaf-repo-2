﻿

# Glosario:
	1. Control de versiones (VC)
	
El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más tarde. De esta manera, evitamos perder la información si es que nuestro computador falla.
[Link](http://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

	2. Control de versiones distribuidos (DVC)
El control de versiones distribuidos deja que los usuarios "clonen" un repositorio, osea que copia toda la información seleccionada. De esta manera, si es que el servidor muere, cualquiera de los clientes que clonaron el repositorio puede restaurarlo.
[Link](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

	3. Repositorio remoto y local
**Repositorio Local:** En contrario a los repositorios remotos, estos son versiones de un proyecto que se encuentran alojados en un computador. 
**Repositorio Remoto:** Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. Puedes tener varios, cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

	4. Copia de trabajo / Working Copy
Se le denomina copia de trabajo o Working copy, al hecho de clonar desde un repositorio todos sus datos. 
[Link](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)

	5. Área de Preparación / Staging Area

Si una versión concreta de un archivo está en el directorio de Git, se considera confirmada (committed). Si ha sufrido cambios desde que se obtuvo del repositorio, pero ha sido añadida al área de preparación, está preparada (staged). Y si ha sufrido cambios desde que se obtuvo del repositorio, pero no se ha preparado, está modificada (modified).
[Link](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

	6. Preparar Cambios / Stage Changes
En el momento en que un usuario hace cambios en un archivo del repositorio y esta seguro de ellos, está preparando los cambios para el archivo. A este estado, se le llama Stage Changes.
[Link 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
[Link 2](https://githowto.com/staging_changes)

	7. Confirmar cambios / Commit Changes

El commit consiste en pasar del estado  Staged a Unmodified. En palabras sencillas, es el momento en que el usuario sube su nuevo archivo al repositor.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

	8. Commit

Es un comando.
Luego de preparar los cambios, se les hace un commit. En simples palabras es como "guardar" los cambios a nuestra rama de árbol local.
[Link](https://git-scm.com/docs/git-commit)

	9. Clone
 Es cuando guardo desde un repositorio local o remoto algún archivo, tomando todos sus datos tal cual está en el repositorio. Este comando nos deja tener todo el repositorio dentro de nuestro PC.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

	10. Pull
Para poder fusionar todos los cambios que se han hecho en el repositorio local.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	11. Push
Envía los cambios que se han hecho en la rama principal de los repertorios remotos que están asociados con el directorio que está trabajando.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	12. Fetch
Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que está trabajando.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	13. Merge
Se usa para fusionar una rama con otra rama activa. 
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	14. Status
Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que están por ser añadidos o comprometidos.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	15. Log
Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	16. Checkout
El comando checkout se puede usar para crear ramas o cambiar entre ellas.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	17. Rama / Branch
Es una linea de trabajo.
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)

	18. Etiqueta / Tag
Se usa para marcar commits específicos. 
[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
