# Ejercicio 3
a) Cantidad y nombre de los branches:

- 1: master

b) Cantidad y nombre de las etiquetas:

- 0: No existen etiquetas

c) Nombre, mensaje y código hash de los últimos 3 commits:

1. Nombre: Correción glosario.md error de link en Fetch.
 Mensaje: Se corrige error en link del concepto Fetch, este ocurría
    por un carácter extra que estaba presente al final de la cita
    a la fuente, provocando con ello que no se pudiese acceder.
    Hash: commit 182e9282af23ce17017f6c7bf02a97ca1f82fa52
2. Nombre:  Actualizacion del glosario.
Mensaje: n.a.
Hash: commit 1f5cd871c6a66120a8b64f6a4b514bdd5a52db97
3. Nombre: Se agrega el archivo glosario.md - 19274811-9
Mensaje: n.a.
Hash: commit 0a291f26d179364e1a8230d3d2a1995bf6462b8a