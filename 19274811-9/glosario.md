
# Glosario
#### Glosario de conceptos y terminos técnicos asociados a git.

1. Control de versiones (VC)


El sistema de control de versiones usa la combinación de tecnologías y prácticas para mantener un registro controlado de los cambios realizados en los archivos que componen un determinado proyecto, lo más común es mantener este registro de cambios especialmente del código de fuente de dicho proyecto, su documentación y otros archivos.

[Fuente](https://producingoss.com/es/vc.html)

2. Control de versiones distribuido (DVC)

El control de versiones distribuido le permite a los desarrolladores trabajar en un mismo proyecto sin la necesidad de estar conectados a una misma red. Las revisiones quedan almacenadas en un sistema de control de versiones distribuido, en lugar de un enfoque cliente-servidor como los sistemas centralizados, la copia que tiene cada cliente es un repositorio completo y el control de versiones distribuido se encarga de sincronizar los repositorios.

[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

3. Repositorio remoto y Repositorio local

Los repositorios remotos con versiones de un proyecto que no se encuentra en tu computador, si no se aloja en internet, estos repositorios pueden ser tanto de solo lectura como también de lectura/escritura, según que permisos tengas. Por otro lado un repositorio local es aquel que se encuentra alojado solamente en tu computador y no se encuentra alojado en internet.

[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

4. Copia de trabajo / Working Copy

La copia de trabajo en Git es la copia del repositorio que tiene un desarrollador para modificar y hacer cambios, desde su perspectiva, la copia de trabajo es un repositorio por sí mismo. Si se necesita, cambios hechos desde un repositorio de acceso publico pueden ser incorporados a esta copia de trabajo, así como los cambios de la copia de trabajo pueden hacerse accesibles a otros como por ejemplo en forma de un parche.

[Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

5. Área de Preparación / Staging Area

El área de preparación es un archivo que generalmente está contenido en tu directorio Git, este archivo contiene la información de lo que va a ir en tu próxima confirmación, tal cómo un índice.

[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

6. Preparar Cambios / Stage Changes

Una versión en el directorio de Git que ha sufrido cambios desde que se obtuvo del repositorio, pero fue añadida al área de preparación, se considera como preparada.

[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

7. Confirmar cambios / Commit Changes

Una versión que ya está en el directorio de Git se dice que está confirmada.

[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

8. Commit

Este comando de Git guarda el contenido actual y los cambios hechos a la versión en el repositorio, usualmente viene acompañado de una breve descripción del usuario detallando los cambios realizados.

[Fuente](https://git-scm.com/docs/git-commit)

9. Clone

Este comando de Git clona un repositorio a un nuevo directorio creado copiando todos sus archivos.

[Fuente](https://git-scm.com/docs/git-clone)

10. Pull

Este comando de Git que incorpora los cambios realizados en un repositorio a su rama actual.

[Fuente](https://git-scm.com/docs/git-pull)

11. Push

Este comando de Git envía al repositorio remoto los cambios realizados en el repositorio local.

[Fuente](https://git-scm.com/docs/git-push)

12. Fetch

Este comando de Git que recupera los commits realizados en el repositorio remoto hacia el repositorio local y los almacena.

[Fuente](https://git-scm.com/docs/git-fetch)

13. Merge

Este comando de Git incorpora cambios desde un commit hacia la rama actual, juntando dos o más historiales de desarrollo juntos.

[Fuente](https://git-scm.com/docs/git-merge)

14. Status

Este comando de Git muestra el estado del árbol de trabajo, específicamente las diferencias existentes entre el archivo índice y el commit HEAD actual.

[Fuente](https://git-scm.com/docs/git-status)

15. Log

Este comando Git muestra el registro de commits hechos en el proyecto.

[Fuente](https://git-scm.com/docs/git-log)

16. Checkout

Este comando Git actualiza los archivos en el árbol de trabajo para que coincida con la versión en el índice del árbol especificado.

[Fuente](https://git-scm.com/docs/git-checkout)

17. Rama / Branch

Una rama puede ser vista como una versión paralela de un repositorio, la cuál está contenida en el mismo repositorio, sin afectar la rama principal, así permitiendo trabajar libremente sin alterar la versión actual del proyecto.

[Fuente](https://help.github.com/articles/github-glossary/)

18. Etiqueta / Tag

La etiquetas en Git son usadas para marcar puntos específicos en la historia como importantes. Generalmente es usado para marcar puntos donde se ha lanzado una nueva versión del proyecto.

[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
