﻿# Ejercicio 3
1. Cantidad y nombre de los branches
+ Cantidad: 1
+ Nombre [1]: master
***
2. Cantidad y nombre de las etiquetas
+ Cantidad: 0
+ Nombres: -
***
3. Nombre, mensaje y código hash de los últimos 3 commits:

+ Nombre:  Franco Ignacio Ortiz Cabrera
+ Mensaje:  Correccion de ejecicio 3 inciso c)
+ Codigo Hash: 2d357ef404dbcee34d206e0c3a5ad4402a50f66b
***
+ Nombre: Jaime Aguilera
+ Mensaje:  .-Correcion ejercicio 3, ultimos 3 commit
+ Codigo Hash: 9757bc3e95df51265c1ac58f3e22213eaecd7681
***
+ Nombre: Alex Órdenes
+ Mensaje:  Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-2
+ Codigo Hash: 50232a998d8b39897174d7e13946bdf561719dac
