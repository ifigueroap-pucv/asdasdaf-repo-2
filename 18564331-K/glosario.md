﻿# Glosario
1. Control de versiones (VC)
+ El **control de versiones** es un registro que funciona como *historial* de las modificaciones de un archivo, permitiendo recuperar versiones anteriores.
[Fuente online](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
***
2. Control de versiones distribuido (DVC)
+ En el **control de versiones distribuidos** se permite obtener una **copia de repositorio completo**, es decir de todo el registro.
[Fuente online](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
***
3. Repositorio remoto y Repositorio local
+ Un **repositorio remoto** es uno que se encuentra alojado en *internet*, mientras que un **repositorio** local se aloja en el *propio equipo*.
[Fuente online](http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github/)
***
4. Copia de trabajo / Working Copy
+ Es una **copia de alguna versión** del repositorio en que se está trabajando.
[Fuente online](https://www.quora.com/Does-the-phrase-working-copy-have-a-different-meaning-from-local-repository-in-version-control-systems)
***
5. Área de Preparación / Staging Area
+ El **área de preparación** es donde se suben los cambios antes de hacerlos en el repositorio, es decir, antes de hacer *commit*.
[Fuente online]()
***
6. Preparar Cambios / Stage Changes
+ Se encarga de **agregar los cambios** al *área de preparación*.
[Fuente online](https://githowto.com/staging_changes)
***
7. Confirmar cambios / Commit Changes
+ **Confirma los cambios** agregados en el *área de preparación* para subirlos al repositorio.
[Fuente online](https://try.github.io/levels/1/challenges/8)
***
8. Commit
+ Es la acción de **guardar una nueva copia** con los cambios que fueron hechos en el repositorio, es decir, *actualizarlo*.
[Fuente online](https://help.github.com/articles/github-glossary/)
***
9. Clone
+ Es una **copia del repositorio** que se encuentra en el propio *equipo*, por lo que **no necesita de estar conectado** para utilizarse, sólo para actualizar al oficial.
[Fuente online](https://help.github.com/articles/github-glossary/)
 ***
10. Pull
+ Funciona para **actualizar el repositorio local** con los cambios hechos en el repositorio remoto.
[Fuente online](https://help.github.com/articles/github-glossary/)
***
11. Push
+ Funciona para **actualizar el repositorio remoto** con los cambios hechos en el repositorio local.
[Fuente online](https://help.github.com/articles/github-glossary/)
    ***
12. Fetch
+ Funciona para **obtener una copia** de los **últimos cambios** hechos en el repositorio *sin modificar* la copia que uno tiene.
[Fuente online](https://help.github.com/articles/github-glossary/)
***
13. Merge
+ Funciona para **combinar los cambios** hechos en un repositorio con el repositorio existente.
[Fuente online](https://help.github.com/articles/github-glossary/)
 ***
14. Status
+ Funciona para **mostrar las diferencias** que se han hecho con el *repositorio oficial* antes de modificarlo.
[Fuente online](https://git-scm.com/docs/git-status)
***
15. Log
+ Funciona para **mostrar el historial** de todos los *commit* hechos en el repositorio.
[Fuente online](https://git-scm.com/docs/git-log)
***
16. Checkout
+ Funciona para **obtener versiones anteriores** del *árbol de trabajo* o **cambiar de ramas**.
[Fuente online](https://git-scm.com/docs/git-checkout)
***
17. Rama / Branch
+ Es una **versión alterna** del *repositorio* en el que se puede trabajar sin modificar la versión oficial.
[Fuente online](https://help.github.com/articles/github-glossary/)
***
18. Etiqueta / Tag
+ Funciona para **etiquetar** alguna **parte del historial** del repositorio.
[Fuente online](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
