# Glosario Git

1. Control de versiones (CV): El Control de versiones es un sistema o una herramienta que permite a los usuarios almacenar los cambios realizados a sus proyectos o archivos de trabajos que esté realizando, esto permite al usuario poder volver a una versión anterior en caso de cometer algún error en la versión actual, lo que le da al usuario una estructura mas ordenada. Pero no solo los archivos o proyectos de un usuario pueden tener un *Control de versiones* sino que, cualquier programa que este posea puede integrarse bajo este sistema de control de versiones ya que, hoy en día varios programa generan su propio control de versiones en caso de que alguna actualización genere un error, el usuario podrá volver sin problema a una versión anterior.
Fuente: [git-scm- Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones),[hipertextua-Qué es un sistema de control de versiones y por qué es tan importante](https://hipertextual.com/archivo/2014/04/sistema-control-versiones/).

2. Control de versiones Distribuido (DVC): El Control de versiones distribuido permite a los usuarios que hayan perdido la conexión con el servidor o que este se haya caído, obtener una copia total del repositorio en su computador permitiéndole al usuario poder seguir con su trabajo.
Fuente: [git-scmAcerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones),[tuprogramacion-GIT, el control de versiones distribuido](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/).

3. Repositorio remoto y Repositorio local: Un repositorio local consiste en tener almacenados las versiones de un proyecto o archivo dentro de tu computador, en cambio el repositorio remoto consiste en almacenar las versiones en otros lugares ya sea base de dato u otro tipo de almacenamiento remoto como GitHub entre otros.
    Fuente: [colaboratorio-Git y GitHub. Trabajando con repositorios locales](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/).

4. Copia de trabajo / *Working copy*: Todo usuario que este que este trabajando en un repositorio posee una copia, por lo que cada usuario podrá trabajar con facilidad su propia parte en donde el control de versiones evita que el trabajo se pierda o se sobrescriba.
Fuente: [thomas-kren-Git Basic Termsn](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms).

5. Área de preparación / *Staging Area*: El Área de preparación o Staging Area es uno de los 3 estado en que se pueden encontrar nuestros archivos. Un Staging Area es donde se guarda la información de lo que se mandara en la próxima confirmación.
Fuente: [git-scm- Fundamentos de Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git).

6. Preparar Cambios/ *Stage Changes*: Es el proceso previo a la confirmación de cambios, es en donde las modificaciones que se le realizaron a un archivo se le aplican el comando *git add* para dejarlo prepara y listo para su confinación.
Fuente: [git-scm-Confirmando tus cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios).

7. Confirmar cambios/*Commit Changes*: Es el momento en que una vez finalizada el área de preparación, se proceden a confirmar estos cambios. Para que esto toda la información debe estar preparada, en caso contrario no será incluida en la confirmacion.Una vez finalizada la confirmación el comando commit dará cierta información de que rama se ha confirmado, cuantos archivos se modificaron, entre otras informaciones. En el caso de los archivos que no fueron preparados siguen estando modificado por lo que se le pueden realizar la preparación y posterior confirmación.
Fuente: [git-scm-Confirmando tus cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios).

8. *Commit*: Commit consiste en guardar o actualizar los cambios hechos a un  archivos en un repositorio remoto o local.
Fuente: [codigofacilito-Commits](https://codigofacilito.com/articulos/commits-administrar-tu-repositorio).

9. *Clone*: Clone es una linea de comando que permite crear una copia local en nuestro computador del repositorio en que se esté trabajando en un nuevo directorio previamente creado.
Fuente: [git-scm-Obteniendo un repositorio Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git),[hostinger-Comandos básicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git).

10. *Pull*: El comando pull permite actualizar el repositorio local. Además Permite fusionar todos los cambios que se han hecho en el repositorio local.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24).

11. *Push*: Push nos permite enviar cambios que se hayan realizados en la rama principal de los repositorios remotos. Es decir lo utilizamos para publicar nuevas confirmaciones locales en un servidor remoto.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24),[hostinger-Comandos básicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git).

12. *Fetch*: Fetch nos permite actualizar nuestro repositorio local a través del repositorio remoto, es decir agrega o actualiza archivos que no estén en el repositorio local y que si se hayan agregado al repositorio remoto.
Fuente: [Tutorial de Git – 21. Fetch y pull rebase](https://www.youtube.com/watch?v=r-PTFYVmGNo),[hostinger-Comandos básicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

13. *Merge*: Comando que permite fusionar distintas ramas activas, esta fusión no siempre resulta perfecta, por lo que el usuario deberá encargar de fusionar manualmente los cambios.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24),[hostinger-Comandos básicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

14. *Status*: Comando que permite visualizar el estado de la carpeta en que se está trabajando.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24).

15. *Log*: El comando Log nos permite visualizar el registro de los commit realizados y todos sus detalles.
Fuente: [hostinger-Comandos básicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git).

16. *Checkout*: El comando Checkout nos permite eliminar los cambios realizados desde el ultimo commit.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24).

17. *Rama*/*Branch*: Este comando se puede utilizarpara crear, borrar, modificar o lista ramas. En donde una rama o Branch es una línea de desarrollo aparte de la rama principal.
Fuente: ["Te enseño git en 30 minutos"](https://www.youtube.com/watch?v=QGKTdL7GG24),[git-scm.com-¿Qué es una rama?](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
18. *Etiqueta*/*Tag*: La Etiqueta o tag en *Git* nos permite etiquetar o marcar el estado de un repositorio, es decir nos permite indicar las versiones o releases de un proyecto o archivo.
Fuente: [git-scm-Creando etiquetas
](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
