﻿
# Glosario



a. **Control de versiones (vc):** el control de versiones registra los cambios realizados sobre un archivo o conjunto de archivos mediante el tiempo, con un sistema de calendarizacion de los elementos introducidos al vc; se pueden recuperar los cambios realizados en un punto especifico, o  ademas retornar los cambios realizados; puedes poner cualquier tipo de archivo bajo el controlador de versiones para registrar sus cambios. Otro punto fundamental en la funcionalidad de este tipo de sistemas es la posibilidad de comparar los cambios realizados entre distintas versiones de un
posible proyecto. [fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

b. **Control de versiones distribuido (dvc):** en un dvc si un cliente descarga un archivo, se copia el repositorio por completo, para así dejar una copia de seguridad por cada vez que descargo el archivo; en el caso si que por alguna razón el servidor muere o borra los archivos estos se copian de algún cliente para restaurar el repositorio. [fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

c. **Repositorio remoto y repositorio local:** un repositorio es una fuente centralizada de almacenamiento de archivos los cuales pueden ser modificados, un repositorio local es el que se crea directamente en tu computador, y un repositorio remoto es externo a este, el cual puede estar en cualquier parte, tanto como en github, o cualquier servidor disponible; en un repositorio remoto se alojan versiones de tus archivos o proyecto. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

d. **Copia de seguridad:** una copia de seguridad asegura que tus datos no sean eliminados en caso de que el servidor en cual están alojados muera, para crear copias de seguridad existen diferentes formas de hacerlo, como en el dvc donde se crea una copia cada vez que se descarga el repositorio por parte de un cliente y otro servidor. [fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

e. **Área de preparación:** el área de preparación es un archivo sencillo en donde se almacenan temporalmente los archivos antes de la próxima confirmación. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

f. **Preparar cambios:** para realizar modificaciones en un archivo que esta bajo seguimiento, no basta solo con modificar el archivo ademas este de debe preparar para la modificación con el comando git add, en caso de que se vuelva a abrir el archivo es necesario volver a prepararlo antes de confirmarlo para que las modificaciones realizadas después de la nueva apertura queden registradas. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

g. **Confirmar cambios:** al momento de confirmar cambios se da informaron respecto a los cambios realizados; a que rama has confirmado;cual es su suma de comprobación;cuantos archivos se modificaron. Ademas es importante considerar que solo se realizan los cambios de los archivos que están en la zona de preparación. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

h. **commit:** comando que se utiliza para confirmar los cambios. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

i. **clone:** clona todos los archivos del repositorio, si se corrompe el disco se pueden recuperar los archivos. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

j. **pull:** actualiza los archivos del repositorio. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

k.  **push:** guarda los archivos del repositorio local en uno remoto.[fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

l.  **Fetch:** recupera toda la información del repositorio desde que lo clonaste por ultima vez. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

m. **Merge:** incorpora los cambios a la rama master para ponerlos en produccion. [fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

n.   **Status:** muestra el estado de los archivos que hay en el directorio, y los que ha en seguimiento para confirmar. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

o. **Log:** se pueden ver las modificaciones que se han llevado a cabo, cuando ya se han realizado confirmaciones [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

p. **Checkout:** crea a una nueva rama y avanzas a ella.[fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

q. **Rama:**  es un apuntador móvil que apunta a una copia especifica que se crea posterior a una confirmación, entre las ramas la primera que se crea es la rama máster que apunta a la ultima confirmación realizada, es decir los cambios mas recientes.[fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

r. **Etiqueta:** se ocupa para etiquetar o marcar algún punto importante o especifico dentro de las modificaciones realizas en un repositorio. [fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)