#Glosario

* **Control de versiones (VC)**
: El control de versiones es un sistema que guarda los cambios realizados sobre uno archivo o mas archivos a lo largo del tiempo, de modo que se pueda recuperar cualquier versión específica más adelante. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "git-scm.com")

* **Control de versiones distribuido (DVC)**
: El control de versiones distribuido permite a diferentes usuarios trabajar en un proyecto común sin necesidad de compartir una misma red. [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido "es.wikipedia.org")

* **Repositorio remoto y Repositorio local**
: Repositorio es el sitio donde estarán guardadas las diferentes versiones del proyecto en el que se está trabajando. El repositorio remoto guardara en un lugar del Internet, por lo que se puede usar para trabajar con varias personas. En el caso del repositorio local, el lugar a guardar será en el PC que se está usando. [Fuente 1][1], [Fuente 2][2], [Fuente 3][3]

 [1]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "git-scm.com"
 [2]: https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/ "colaboratorio.net"
 [3]: https://colaboratorio.net/atareao/developer/2017/git-github-trabajando-repositorios-remotos/ "colaboratorio.net"

* **Copia de trabajo / Working Copy**
: Working copy es una copia de los archivos del repositorio y donde se realizan los cambios que se quieran hacer. [Fuente](https://www.osmosislatina.com/cvs_info/wcopy.htm "osmosislatina.com")

* **Área de Preparación / Staging Area**
: Staging Area es un archivo que contiene los cambios preparados pero no confirmados.
[Fuente 1][1], [Fuente 2][2]

 [1]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git "git-scm.com"
 [2]: http://gitready.com/beginner/2009/01/18/the-staging-area.html "gitready.com"

* **Preparar Cambios / Stage Changes**
: Stage changes prepara los cambios hechos, añadiéndolos al área de preparación. [Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio "git-scm.com")

* **Confirmar cambios / Commit Changes**
: Commit changes confirma los cambios hechos que están en el área de preparación, guardándolos en el repositorio local. [Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio "git-scm.com")

* **Commit**
: Commit es un comando que añade los últimos cambios hechos y preparados al repositorio local(es posible hacerlo sin preparar). [Fuente](https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push "stackoverflow.com")

* **clone**
: Clone es un comando usado para crear una copia de un repositorio. [Fuente](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone "atlassian.com")

* **pull**
: Pull es un comando que se usa para añadir cambios en un repositorio remoto al repositorio local y además hace cambios al working copy. [Fuente 1][1], [Fuente 2][2]

 [1]: https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull "git-tower.com"
 [2]: https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch "stackoverflow.com"

* **push**
: Push es un comando que se usa para añadir cambios en un repositorio local al repositorio remoto. [Fuente](https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push "stackoverflow.com")

* **fetch**
: Pull es un comando que se usa para añadir cambios en un repositorio remoto al repositorio local sin hacer cambios al working copy. [Fuente 1][1], [Fuente 2][2]

 [1]: https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull "git-tower.com"
 [2]: https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch "stackoverflow.com"

* **merge**
: Merge es un comando que combina diferentes colecciones de archivos en una sola con ambos conjuntos de cambios.

* **status**
: Status es un comando que muestra el estado actual del working copy y staging area. Muestra cuales son los cambios que están preparados, cuales no lo están y que archivos no están siendo rastreados. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository "atlassian.com")

* **log**
: Log es un comando que muestra un historial de commit hechos hasta el momento, es decir, muestra una lista en detalle de las veces que se ha hecho commit desde el más nuevo hasta el más antiguo. [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History "git-scm.com")

* **checkout**
: Checkout es un comando que se usa para cambiar entre Ramas de un repositorio que se posee. [Fuente](https://stackoverflow.com/questions/7298598/what-is-the-difference-between-git-clone-and-checkout "stackoverflow.com")


* **Rama / Branch**
: Branch es una separación del trabajo principal que se usa para no tener que "dañarlo"(puede ser mas de una separación). Se puede trabajar en las separaciones de forma paralela. [Fuente 1][1], [Fuente 2][2]

 [1]: https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell "git-scm.com"
 [2]: https://www.atlassian.com/git/tutorials/using-branches "atlassian.com"

* **Etiqueta / Tag**
: Tag es una marca que se usa para indicar puntos en el historial como importantes. [Fuente](https://git-scm.com/book/id/v2/Git-Basics-Tagging "git-scm.com")