1. Cantidad y nombre de la branches: se debe escribir: git branch. El resultado es: *master*. Existe una rama.
2. Cantidad y nombre de las etiquetas: se debe escribir: git tag. No existen etiquetas.
3. Nombre, mensaje y código hash de los últimos 3 commits. Use el comando
*"git log"* para obtener esta información:

```
commit a4a5742022a0c4f6310e90eb9a351b0bf5d9247d (HEAD -> master)
Author: csc1234 <cristobalcesar.sanchez@gmail.com>
Date:   Wed Mar 7 12:54:36 2018 -0300

    haciendo un commit

commit 2df891c40221c86f61bac143b94ce66e847a0d80
Author: csc1234 <cristobalcesar.sanchez@gmail.com>
Date:   Wed Mar 7 12:50:42 2018 -0300

    Se creo la carpeta 191516095, conteniendo el glosario sobre git.

commit ea8a3aeb490414d1a855689aa0b805c2e623458d (origin/master, origin/HEAD)
Author: Ismael Figueroa <ismael.figueroa@pucv.cl>
Date:   Tue Mar 6 02:57:33 2018 +0000

    README.md created online with Bitbucket
```