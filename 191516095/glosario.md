1. Control de versiones (VC): Es un sistema que registra los cambios realizados sobre un archivo a lo largo del tiempo, permitiendo la recuperar la versión que se desee.
2. Control de versiones distribuido (DVC): Permite una restauración del código de la aplicación en caso de perdida, debido a la existencia de un controlador de versiones.
3. Repositorio remoto y Repositorio local: Un repositorio es el lugar donde se almacena una o varias versiones de un proyecto. Existen el local, se modifica en el computador que utiliza el desarrollador, y remoto, las son guardadas en un servidor por internet.
4. Copia de trabajo / *Working Copy*: El proyecto es guardado antes de realizar un commit.
5. Área de Preparación / *Staging Area*: Puede ser un editor que permita hacer cambios en el proyecto.
6. Preparar Cambios / *Stage Changes*: Explicar los cambios realizados.
7. Confirmar cambios / *Commit Changes*: Realizar un commit al repositorio.
8. *Commit*: Comando que hace efectivos los cambios realizados localmente.
9. *clone*: Comando utilizado para copiar un repositorio remoto a uno local.
10. *pull*: Comando que actualiza un repositorio local a los cambios hechos recientemente.
11. *push*: Comando que permite subir una rama a un repositorio remoto.
12. *fetch*: Comando que deshace todos los cambios locales y recientes.
13. *merge*: Comando que une fusiona otra rama a una activa.
14. *status*: Comando utilizado para inspeccionar el trabajo realizado en un repositorio.
15. *log*: Comando utilizado para obtener el nombre, mensaje e id de los commits.
16. *checkout*: Comando que permite el cambio de rama.
17. Rama / *Branch*: Se utilizan para desarrollar funcionalidades aisladas unas de otras. También es un comando utlizado para crear ramas.
18. Etiqueta / *Tag*: se usa típicamente para marcar versiones de lanzamiento.

Algunas definiciones fueron obtenidas de:
1. https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
2. http://www.bibliotecas.udec.cl/?q=content/%C2%BFqu%C3%A9-es-un-repositorio
3. https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado
4. http://rogerdudler.github.io/git-guide/index.es.html
5. https://www.atlassian.com/git/tutorials/inspecting-a-repository
