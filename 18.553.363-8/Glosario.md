﻿
# GLOSARIO TALLER 0 DE GIT

## Terminos

### _a. Control de versiones (VC)_:
El control de versiones es un sistema que registra los cambios realizados sobre un archivo o un conjunto de archivos a lo largo del tiempo, permitiendo recuperar versiones especificas.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
###  _b. Control de versiones distribuido (DVC)_
El control de versiones distribuido es un sistema que replica los cambios realizados sobre un archivo o conjunto de estos de modo que los clientes tengan una copia de seguridad en caso de requerir restaurar el sistema 
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
###  _c. Repositorio remoto y Repositorio local_ 
Los repositorios locales es  una copia directa de los servidores con los cuales trabaja un un equipo físico, como a su vez existen los repositorios remotos que permiten tener una copia de estos en una nube en caso de necesitar restaurarlo
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
####  _d. Copia de trabajo / Working Copy_ 
Las copias de trabajo son una copia de seguridad en caso de ser necesaria la restauración del sistema en caso de perdida del servidor
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
####  _e. Área de Preparación / Staging Area_ 
Es un modo interactivo de Git (script) en el cual podremos introducir archivos que queramos ir (añadiendo,sacando o modificando) a nuestro sistema 
[Fuente](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva)
####  _f. Preparar Cambios / Stage Changes_ 
La forma en la cual podremos cambios en nuestros archivos de Git sera a travez de la sentencia "git add"
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
####  _g. Confirmar cambios / Commit Changes_ 
El commit changes es la confirmación de los cambios. Al utilizar Git Status, a diferencia del Commit, el commit changes nos mostrara los cambios efectuados. Es posible también ampliarla a la confirmatorio de grupos, la forma comúnmente utilizada para referirse a estos cambios es indicando su código-resumen criptográfico SHA-1, 
[Fuente](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Selecci%C3%B3n-de-confirmaciones-de-cambios-concretas)
####  _h. Commit_ 
Es la manera de llamar a una confirmación de cambios ya ingresada al sistema 
[Fuente](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Selecci%C3%B3n-de-confirmaciones-de-cambios-concretas)
####  _i. clone_
Es el comando utilizado para copiar un repositorio local o remoto
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
####  _j. pull_
Descarga los cambios relizados a tu repositorio local 
[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git/248)
####  _k. push_
Es un comando utilizado para enviar el estado actual de tu proyecto a quien quieres compartilo "Enviandolo a un repositorio remoto" 
[Fuente](https://elweb.co/git-tutorial-1-en-espanol/)
####  _l. fetch_
Consulta los cambios que hay en el repositorio con respecto a tu copia local
[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git/248)
####  _m. merge_
La sentencia git merge se utiliza para fusionar uno o más ramas dentro de la rama que tienes activa
[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)
####  _n. status_ 
Muestra el estado actual de los commits que se han realizado en el repositorio
[Fuente](https://elweb.co/git-tutorial-1-en-espanol/)

####  _o. log_
Es usado para mostrar la historia registrada alcanzable de un proyecto desde la más reciente confirmada hacia atrás
[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)

####  _p. checkout_
Es usado para cambiar de rama y revisar el contenido de tu directorio de trabajo.
[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)
####  _q. Rama / Branch_
Una rama es un apuntador móvil apuntando a una de esas confirmaciones. La rama por defecto de Git es la rama master
[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)
####  _r. Etiqueta / Tag_
Se utiliza para dar un marcador permanente a un punto específico en el historial del código fuente. Generalmente esto se utiliza para cosas como las liberaciones
[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)




























> Written with [StackEdit](https://stackedit.io/).
