# Glosario

 1. Control de versiones (VC)

    Un control de versiones a grandes razgos, es tomar imagenes de los documentos durante la creación. Es tener puntos de respaldo a travez del tiempo, los cuales indican cuando se tomaron y que cambios se realizaron.
[Fuente](https://programminghistorian.org/es/lecciones/introduccion-control-versiones-github-desktop#qu%C3%A9-es-un-control-de-versiones-y-por-qu%C3%A9-deber%C3%ADa-utilizarlo)
 2. Control de versiones distribuido (DVC)

    Los distintos usuarios conectados mantienen una replica completa del repositorio, por lo que si se pierde la copia del servidor, se puede reponer desde uno de los usuarios.
	[Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)
 3. Repositorio remoto y Repositorio local
	Repositorios remotos son los que están alojados en cualquier parte de internet, de la cuál puedes tener una o más copias.
	Repositorio local es el que se encuentra en el computador del usuario.
	[Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

 4. Copia de trabajo / Working Copy
	El directorio de trabajo es una copia de una versión del proyecto. Estos archivos se sacan de la base de datos comprimida en el directorio de Git, y se colocan en disco para que los puedas usar o modificar.

 5. Área de Preparación / Staging Area
El área de preparación es un archivo simple, que se encuentra en el directorio de Git, y almacena información acerca de lo que va a ir en la próxima confirmación.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
 6. Preparar Cambios / Stage Changes
Cuando un archivo modificado se agrega al area de preparación.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
 7. Confirmar cambios / Commit Changes
Confirmar los cambios producidos en la Staging Area, pasan al estado "sin modificar"
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
 8. Commit
Comando para confirmar los cambios en el archivo que esta en la stage area
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
 9. clone 
Copia un repositorio Git dentro de un directorio recién creado. Se descarga cada versión de cada archivo en la historia del proyecto.
[Fuente](https://git-scm.com/docs/git-clone)
 11. pull 
 Al ejecutar "git pull", por lo general se recupera la información del servidor clonado, y automáticamente se intenta unir con el código con en el que se está trabajando actualmente.
 [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
 12. push 
 Envía el proyecto actual a un repositorio remoto para compartir la información. 
 Este comando funciona únicamente si se ha clonado de un servidor con permiso de escritura, y nadie ha enviado información mientras tanto. Si dos personas clonan a la vez, y el primero envía su información y el segundo envía la suya, su envío será rechazado. Tendrá que bajarse primero el trabajo de la primera persona e incorporarlo en el suyo para que se le permita hacer un envío.
 [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
 
14. fetch
 Descarga los cambios que no poseas desde repositorio remoto a la rama origin/master. No une la información con el trabajo realizado, ni modifica lo que estás trabajando, se debe realizar posteriormente.
  [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-básicos-para-ramificar-y-fusionar)
 15. merge  
 Incorporar los cambios descargados con el trabajo local
 [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-básicos-para-ramificar-y-fusionar)
 16. status  
 Muestra el estatus del arbol de trabajo, donde se encuentran todos los archivos en conflicto, o archivos que esperan un commit.
 [Fuente](https://git-scm.com/docs/git-status/1.8.1)
 17. log 
Muestra el historico de confirmaciones, con las modificaciones que se han llevado a cabo en orden cronologico inverso.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-histórico-de-confirmaciones)
 19. checkout 
Crea una nueva rama, y se posiciona en ella.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-básicos-para-ramificar-y-fusionar)
 21. Rama / Branch  
 Las ramas son caminos que puede tomar el desarrollo de un software, permiten que nuestro proyecto pueda tener diversos estados y que los desarrolladores sean capaces de pasar de uno a otro de una manera ágil.
 [Fuente](https://desarrolloweb.com/articulos/trabajar-ramas-git.html)
 22. Etiqueta / Tag
	Git tiene la habilidad de etiquetar (tag) puntos específicos en la historia como importantes. Generalmente la gente usa esta funcionalidad para marcar puntos donde se ha lanzado alguna versión
	[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
