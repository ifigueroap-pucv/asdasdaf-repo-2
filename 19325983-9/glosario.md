# Glosario
a. Control de versiones (VC):
    Se llama **control de versiones** a la gestión de los diversos cambios que se realizan sobre los elementos de algún 	producto o una configuración del mismo. Una versión, revisión o edición de un producto, es el estado en el que se encuentra el mismo en un momento dado de su desarrollo o modificación.
    [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)
    
b. Control de versiones distribuido (DVC):
	En programación informática, el **control de versiones distribuido** permite a muchos desarrolladores de software trabajar en un proyecto común sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido (**DVCS**, por sus siglas en inglés).
	[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)
	
c. Repositorio remoto y Repositorio local:
    Un repositorio es un almacén virtual destinado a mantener las versiones de un programa o información, existen dos tipos, el local el cual corresponde al que esta dentro del equipo del usuario, y el remoto que esta ubicado en un servidor al cual pueden acceder varios usuarios.
    [Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
    
d. Copia de trabajo / Working Copy:
	Corresponde a la copia local donde el usuario trabaja.
	[Fuente](https://www.youtube.com/watch?v=QGKTdL7GG24)
	
e. Área de Preparación / Staging Area
	Esta área es un espacio orientado a almacenar la información proveniente de nuestro sistema operacional o de otras fuentes, con una vida temporal o no, que será el punto de partida de los procesos de depuración, transformación y carga del proyecto.
	[Fuente](https://churriwifi.wordpress.com/2010/05/07/16-2-definicion-area-stage-tecnicas-etl/)
	
f. Preparar Cambios / Stage Changes
	 Esto significa que git sabe sobre el cambio, pero no es permanente en el repositorio.
     [Fuente](https://githowto.com/staging_changes)
     
g. Confirmar cambios / Commit Changes
	marcar un punto de envio al repositorio local sin subirlo al repositorio remoto.
    [Fuente](https://githowto.com/staging_and_committing)

h. Commit:
	permite guardar los cambios en el repositorio.
    [Fuente](https://git-scm.com/docs/git-commit)
    
i. clone:
	Clona un repositorio a un nuevo directorio.
    [Fuente](https://git-scm.com/docs/git-clone) 
	
j. pull:
	Busca e integra con otro repositorio o una rama local.
    [Fuente](https://git-scm.com/docs/git-pull)
	
k. push:
	Actualiza referencias remotas junto con objetos asociados.
    [Fuente](https://git-scm.com/docs/git-push)
	
l. fetch:
	Descarga objetos y referencias desde otro repositorio.
    [Fuente](https://git-scm.com/docs/git-fetch)
	
m. merge:
	Une dos o más historias de desarrollo juntas.
    [Fuente](https://git-scm.com/docs/git-merge)
	
n. status:
	Muestra los parámetros del árbol de trabajo.
    [Fuente]( https://git-scm.com/docs/git-status)
	
o. log:
	Muestra el historial de commits.
    [Fuente](https://git-scm.com/docs/git-log)
	
p. checkout:
	Cambiar ramas o restaurar archivos de árbol de trabajo.
    [Fuente](https://git-scm.com/docs/git-checkout)
	
q. Rama / Branch:
	Una rama es un repositorio unido al repositorio principal el cual no causa cambios inmediatos a este ultimo evitando los errores al realizar la conbinacion de distintas areas de un proyecto.
    [Fuente](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
	
r. Etiqueta / Tag:
	Crea, lista, elimina o verifica un objeto etiquetado firmado con GPG.
    [Fuente](https://git-scm.com/docs/git-tag)
	