a. Cantidad y nombre de los branches:
 	1 branch : master
    
b. Cantidad y nombre de las etiquetas:
	0.
    
c. Nombre, mensaje y código hash de los últimos 3 commits. Use el comando git log:

Commit Hash 8b1aab115fb7aea469ef9859bc8f98fa61ab3d3f (HEAD -> master)
Nombre: Alex Órdenes <alex.ord.lei@gmail.com>
Fecha  Mon Mar 12 15:56:55 2018 -0300

Commit Hash 4035b54d5516305dd02ee07827b8db19497ca917 (origin/master, origin/HEAD)
Nombre: Cristobal Vargas Goycolea <cristobal.m.vargas@gmail.com>
Fecha:   Mon Mar 12 01:01:25 2018 -0300

Commit Hash 18de85e0fc5e31b42a4d557c89b7506bad9095b2
Nombre: Cristobal Vargas Goycolea <cristobal.m.vargas@gmail.com>
Fecha:   Mon Mar 12 00:50:29 2018 -0300
	