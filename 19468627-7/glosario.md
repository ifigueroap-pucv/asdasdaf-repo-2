﻿# Glosario 

 1. **Control de versiones (VC) :** Es una combinación de tecnologías y practicas que registran los cambios realizados sobre archivos a través del tiempo (lo que pueden ser ficheros del proyecto, código fuente, documentacion, etc).
[fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
[fuente 2](https://producingoss.com/es/vc.html)
 2. **Control de versiones distribuido (DVC) :** El control de versiones distribuido cumple con la tarea de registrar cambios realizados sobre uno o múltiples archivos a través del tiempo, lo que permite una recuperación de estos de ser necesario. Su principal característica es que permite a muchas personas colaborar en un mismo proyecto sin necesidad de compartir una red (esto permite modelos jerárquicos). Las personas obtienen una copia completa del repositorio, esto funciona como copia de seguridad para que, en caso de que el servidor principal falle, habrán repositorios que puedan copiarse para restaurarlo.
[fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
 3. **Repositorio remoto y Repositorio local :** Un repositorio local es aquel que se encuentra en nuestra computadora, puede ya sea ser creado desde cero o ser un clon de un repositorio remoto, en este podemos realizar cambios a nivel local.
Por otro lado, el repositorio remoto es aquel que se encuentra alojado en algún punto de la red y que necesita que el usuario disponga de los permisos necesarios para velo/editarlo.
[fuente 1](https://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows/)
[fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[fuente 3](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
[fuente 4](https://colaboratorio.net/atareao/developer/2017/git-github-trabajando-repositorios-remotos/)

 4. **Copia de trabajo / Working Copy :** Los archivos de un repositorio no pueden ser editados de manera directa, por lo que se debe obtener una copia de estos para poder trabajar con ellos, esta "copia" que alojamos en el computador se denomina copia de trabajo y tiene un vinculo permanente a la dirección original de la carpeta en el repositorio.
 [fuente 1](https://cornerstone.assembla.com/cornerstone/helpbook/pages/introduction/terminology/working-copies.html)
 5. **Área de Preparación / Staging Area :** El área de preparación o index, tiene la tarea de almacenar los cambios que se han realizado sobre los archivos y gestionar cuales de ellos serán efectivamente aplicados al repositorio. Es la capa intermedia entre la copia de trabajo y el repositorio y permite un gran control sobre los cambios que se envíen al repositorio.									
[fuente 1](http://gitready.com/beginner/2009/01/18/the-staging-area.html)
[fuente 2](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva)

 6. **Preparar Cambios / Stage Changes :**  Es el proceso mediante el cual las modificaciones hechas sobre un archivo en el área de trabajo son llevadas al área de preparación. Esto quiere decir, que los cambios realizados sobre un fichero son llevados a un estado preparado, en el cual están a la espera de confirmación para ser aplicados al repositorio.
[fuente 1](https://githowto.com/staging_changes) 
 [fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Preparando-archivos-modificados)
 

 7. **Confirmar cambios / Commit Changes :** Es el proceso en donde todos los cambios del área de preparación son aplicados al repositorio. Al terminar este proceso deja información de la rama afectada, cuales archivos se modificaron, etc. 
 [fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios) 
 

 8. **Commit :** Comando de Git. Su función es aplicar los cambios que están en el index hacia el repositorio, ademas adjunta un mensaje escrito por el usuario, el cual describe las modificaciones que se efectuaron.
  [fuente 1](https://git-scm.com/docs/git-commit) 
  

 9. **Clone :** Comando de Git. Su función es clonar un repositorio dentro de un nuevo directorio, donde crea ramas de seguimiento remoto para cada una de las existentes en el repositorio original, con esto se asegura de que luego de clonar, las ramas remotas esten actualizadas y que la rama maestra no se diferencie de la remota.
 [fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) 
 [fuente 2](https://git-scm.com/docs/git-clone)
 

 10. **Pull :** Comando de Git. Su función es incorporar cambios desde un repositorio remoto a la rama en la que se esta ubicando actualmente. En palabras mas simples, extrae la información del servidor actual y la une con el servidor remoto.
 [fuente 1](https://git-scm.com/docs/git-pull) 
 [fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
 
 11. **push :** Comando de Git. Envía el proyecto actual a un repositorio remoto para compartir la información. Este comando funciona únicamente si se ha clonado de un servidor con permiso de escritura, y nadie ha enviado información mientras tanto. Si dos personas clonan a la vez, y el primero envía su información y el segundo envía la suya, su envío será rechazado. Tendrá que bajarse primero el trabajo de la primera persona e incorporarlo en el suyo para que se le permita hacer un envío.
  [fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 
   [fuente 2](https://www.git-tower.com/learn/git/commands/git-push) 
   

 12. **Fetch :** Comando de Git. Su función es obtener datos nuevos que hay en el repositorio remoto, pero no integra nada de esta nueva información a los archivos en los que se este trabajando. Es útil para ver lo que ha pasado en el repositorio en el cual se esta trabajando.
  [fuente 1](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git) 
   [fuente 2](https://git-scm.com/docs/git-fetch) 
   

 13. **Merge :** Comando de Git. Su función es integrar en una única rama líneas de desarrollo que en algún punto se dividieron, permitiendo combinar múltiples secuencias de commits en una única agrupación. Cuando se realiza un merge la rama en la que se este ubicado, esta  se actualizará reflejando la incorporación entre ambas, pero la objetivo no sufrirá cambios.
  [fuente 1](https://www.atlassian.com/git/tutorials/using-branches/git-merge) 
   [fuente 2](https://git-scm.com/docs/git-merge) 
   

 14. **Status :** Comando de Git. Su función es mostrar el estado del árbol de trabajo y del área de preparación. Permite ver los cambios que están preparados para el próximo commit, los que no lo están y los archivos que no están bajo seguimiento por Git. 
[fuente 1](https://www.atlassian.com/git/tutorials/inspecting-a-repository#git-status) 
   [fuente 2](https://git-scm.com/docs/git-status/1.8.1) 
   

 15. **Log :** Comando de Git. Su función es mostrar en detalle todos los commits realizados. Se puede listar la historia del proyecto, filtrarla y buscar por cambios o puntos específicos de este. No tiene formato de salida especifico, ya que este puede ser editado según los parámetros del archivo
[fuente 1](https://git-scm.com/docs/git-log) 
   [fuente 2](https://www.atlassian.com/git/tutorials/inspecting-a-repository#git-log) 
   [fuente 3](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones) 

 16. **Checkout :** Comando de Git. Su función es Actualizar los archivos en el árbol de trabajo para que coincida con la versión en el index o el árbol especificado. Si no se proporcionan rutas, *checkout* también dejara establecida la rama especificada como la rama actual.
 [fuente 1](https://git-scm.com/docs/git-checkout) 
 [fuente 2](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)
   [fuente 3](https://www.atlassian.com/git/tutorials/using-branches/git-checkout) 

 17. **Rama / Branch :** Es un puntero desplazable que esta direccionado a una versión especifica del proyecto. Son utilizadas para crear arboles de trabajo separados, en algunos casos puede ser vista como otro contexto de desarrollo
 [fuente 1](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms) 
 [fuente 2](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

 18. **Etiqueta / Tag :** Es un puntero inmutable (inamovible) que apunta a un commit específico. Existen dos tipos de tags: *ligeras* y *anotadas*. Los tags ligeros solo apuntan a un commit, mientras que el segundo tipo, crea un objeto adicional que es guardado en el repositorio, el cual contiene información asociada al tag. Se usa generalmente para marcar puntos donde se ha lanzado alguna versión (ej: v2.1, v2.1.1,etc.).
 [fuente 1](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) 
 [fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
   [fuente 3](http://alblue.bandlem.com/2011/04/git-tip-of-week-tags.html) 

