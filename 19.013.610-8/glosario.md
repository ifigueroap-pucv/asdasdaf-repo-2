# Taller 0



# Glosario

a. **Control de versiones(VC):** es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo.
**Fuente:**  https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
b. **Control de versiones distribuidos(DVC):**  sistema que registro los cambios sobre un archivo o conjuntos de estos a lo largo del tiempo, los clientes pueden replicar completamente el repositorio.
**Fuente:** https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
c. **Repositorio remoto y local:** son versiones de un proyecto que se encuentran alojados en internet(remoto) o en disco(local).
**Fuente:**https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
d. **Copia de trabajo / Working Copy** copia del proyecto que un miembro del proyecto.
**Fuente:**https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms
e. **Área de Preparación / Staging Area**
**Fuente:**
f. **Preparar Cambios / Stage Changes**
**Fuente:**
g. **Confirmar cambios / Commit Changes**
**Fuente:**
h. **Commit:** se trata de subir archivos al repositorio local .
**Fuente:** https://codigofacilito.com/articulos/commits-administrar-tu-repositorio
i. **clone:** copia un repositorio e un nuevo directorio.
**Fuente:** https://git-scm.com/docs/git-clone
j. **pull** integra los datos nuevos que esten el repositorio remoto en el repositorio local
**Fuente:** https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull
k. **push:** comando para mandar archivos a un repositorio remoto.
**Fuente:** https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git
l. **fetch:** descarga datos nuevos desde el repositorio remoto al repositorio local sin integrarlos en el repositorio local.
**Fuente:**https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull
m. **merge:** junta o fuciona dos ramas 
**Fuente:**https://git-scm.com/docs/git-merge |  https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
n. **status:** muestra el estado del arbol de trabajo.
**Fuente:** https://git-scm.com/docs/git-status
o. **log:** muestra los commits hechos
**Fuente:**https://git-scm.com/docs/git-log
p. **checkout:** intercambia ramas o restaura archivos del arbol de trabajo.
**Fuente:**https://git-scm.com/docs/git-checkout
q. **Rama / Branch:** es una bifurcación del proyecto que se está realizando para añadir una nueva funcionalidad o corregir un bug.
 **Fuente:** https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/
r. **Etiqueta / Tag:** punto en la historia donde se ha resaltado, usualmente usado para marcar versiones 
**Fuente:**https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas




