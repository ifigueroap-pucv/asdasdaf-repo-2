﻿# TALLER 0 INGENIERIA WEB

## Glosario:

#### a. Control de versiones  (VC):

Verificación de cambios realizados sobre algún producto. En la informática consiste en un sistema encargado de registrar los cambios en un archivo o un conjunto de aquellos a lo largo del tiempo.Esto es de gran importancia ya que permitirá, entre otras cosas, comparar cambios durante la realización y ver quién hizo cada cambio


#### b. Control de versiones distribuido (DVC):

Consiste en un control de versiones donde no es necesario un servidor central. Al descargar la ultima versión de los archivos, se realiza una copia completa del repositorio. De esta forma si un servidor falla, habrán respaldos de la información ya que cada programador tiene una copia completa del repositorio en su equipo.

#### c. Repositorio remoto y Repositorio local: 
Un repositorio es una instalación virtual donde se guarda y se maneja información digital. Un repositorio local es el que se tiene en la memoria del ordenador en el cual se trabaja. Un repositorio remoto son versiones de un proyecto que se encuentran guardados en internet.

#### d. Copia de trabajo/*working copy*:
Al clonar un repositorio, se descargará toda la información de este, y se creará una *copia de trabajo*. Esta se trabajará en nuestro repositorio local para después poder re-integrarse al repositorio remoto.

#### e. Area de preparación/*Staging area* 

El área de preparación es un archivo que almacena la información acerca de lo que ira en la próxima confirmación, se le conoce también como indice. Básicamente es como un muelle en el cual uno puede determinar que *cambios* serán enviados.

#### f. Preparar cambios/*Stage changes*:
Esto significa que git reconoce el cambio realizado, sin embargo no es permanente en el repositorio. La próxima confirmación incluirá los cambios 

#### g. Confirmar cambios/*Commit changes*:
Confirmar cambios guarda los cambios realizados y seleccionados en el área de preparación, en el repositorio. Cualquier cosa que no este preparada, no será confirmado; es decir que se mantendrán los archivos en el disco

#### h. *Commit*
Guarda una instantanea del área de preparación.
Imaginemos que construimos algo complicado, como un modelo de avión; y queremos llevar registro de todo lo que avanzamos. 
Cada vez que llegamos a un cierto nivel de progreso, sacamos nuestro celular y tomamos una foto. Así una vez que el modelo este completo podemos retroceder y observar todo nuestro proceso de armado. En este ejemplo cada fotografía es una acción de *commit*

#### i. *Clone*:
Comando utilizado para obtener una copia de un repositorio existente , cada versión de los archivos dentro proyecto es descargado cuando se ejecuta *git clone* .

#### j.  *Pull*:

Actualiza el repositorio local, al *commit* mas reciente, realizara una fusión de los cambios realizados en el repositorio local.

#### k. *Push*: 

Sube los cambios realizados a una rama de trabajo local y/o remota. Si no se ejecuta el comando *push*, los cambios no se veran reflejados en el repositorio remoto.

#### l. *Fetch*:
Descargará los cambios de una rama determinada pera dejarlos en una rama "auxiliar" (origin/master) en la cual se puede tener control de los cambios para, posteriormente, realizar *merge* con la rama local.

#### m. *Merge*:
Fusiona una o más ramas de trabajo dentro de la rama que se encuentra activa. Esto integra las lineas de código de *commits* una con otra.

#### n. *Status*:
Mostrará los estados de los archivos en el área de trabajo y de prueba, también mostrará qué archivos están modificados y cuales no han sido confirmados.

#### o. *Log*:
Comando utilizado para mirar atrás y ver que cambios han sido realizados, esto normalmente se hace después de haber llevado a cabo varias confirmaciones o si se ha clonado un repositorio que contaba con un historial de confirmaciones.

#### p. *Checkout*:
Salta de una rama a otra, es el mecanismo para moverse por los *commit* moviendo el indicador (*head*) al momento deseado. Esto es utilizado para añadir una nueva funcionalidad al proyecto mediante el uso de ramas (*branching*) 

#### q. Rama/*Branch*:
Una rama es una linea independiente de trabajo, es muy útil a la hora de delegarse el trabajo entre desarrolladores (normalmente por áreas). Diferentes ramas pueden fusionarse en cualquiera de ellas. El uso de ramas sirve para aislar nuestro trabajo, ya que los cambios en la rama primaria no afectara la rama en la nos encontramos trabajando.

#### r. Etiqueta/*Tag*:

Se usan las etiquetas para marcar puntos específicos en la historia como importantes o de interés. Estas etiquetas son referencias que no pueden ser cambiadas. Usualmente son usadas para identificar puntos de lanzamiento del proyecto (ej: version 1.0).

## FUENTES:
##### CONTROL DE VERSIONES:
1. https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
2. https://es.wikipedia.org/wiki/Control\_de\_versiones

##### CONTROL DE VERSIONES DISTRIBUIDO:
1. https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
2. http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/

##### REPOSITORIO REMOTO Y REPOSITORIO LOCAL:
1. https://es.wikipedia.org/wiki/Repositorio
2. https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/
3. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

##### COPIA DE TRABAJO:
1. https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms
2. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

##### AREA DE PREPARACION:
1. https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
2. http://gitready.com/beginner/2009/01/18/the-staging-area.html

##### PREPARAR CAMBIOS:
1. https://githowto.com/staging_changes

##### CONFIRMAR CAMBIOS:
1. https://www.quora.com/What-is-the-meaning-of-commit-and-stage-in-git
2. https://www.quora.com/What-does-it-mean-to-commit

##### *COMMIT*:
1. https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio
2. https://www.quora.com/What-does-it-mean-to-commit

##### *CLONE*:

1. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

##### *PULL*:
1. http://rogerdudler.github.io/git-guide/index.es.html
2. https://www.hostinger.es/tutoriales/comandos-de-git

##### *PUSH*:
1. https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

##### *FETCH*:
1. https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git

##### *MERGE*:
1. https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar
2. https://styde.net/ramas-y-resolucion-de-conflictos-en-git/

##### *STATUS*:
1. https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Seguimiento-B%C3%A1sico

##### *LOG*:
1. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones

##### *CHECKOUT*:
1. https://www.genbetadev.com/herramientas/manejo-de-ramas-de-desarrollo-con-git
2. https://stackoverflow.com/questions/15296473/what-do-git-checkouts-really-mean

##### RAMA/*BRANCH*:
1. https://backlog.com/git-tutorial/stepup/stepup1_1.html

##### ETIQUETA/*TAG*:
1. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
2. https://www.quora.com/Why-are-tags-used-in-Git



