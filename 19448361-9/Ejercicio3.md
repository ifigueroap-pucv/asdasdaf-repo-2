﻿## Ejercicio 3 
#### Cantidad  y nombre de los branches:

	* master

| Cant. |Nombre  |
|--|--|
| 1 |Master  |

#### Cantidad de nombre de las etiquetas:

No hay etiquetas.

#### Nombre, mensaje y código *hash* de los últimos 3 *commits*:

	Nombre: Max Chavez
	Mensaje: "se subio el archivo glosario.md
	Codigo hash: 1102ef4bc3f0fc209be1e73ad75feb5d746b5a28
	
	Nombre: Juan Avila
	Mensaje: Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-2
	Codigo hash: 1f03ded4698969fc0f398cb8313d3d2729c33998
	
	Nombre: Juan Avila
	Mensaje: Se agrega el archivo datos.md
	Codigo hash: 9494aaf13d9743a7144fd25162625c9b4e079011
