## Glosario ##



### a. Control de versiones (VC) ###

El control de versiones es un sistema que registra los cambios realizados sobre archivos a lo largo del tiempo, guardando así versiones del producto en todas sus fases del desarrollo.

Fuentes: [https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
[https://hipertextual.com/archivo/2014/04/sistema-control-versiones/](https://hipertextual.com/archivo/2014/04/sistema-control-versiones/)

###b. Control de versiones distribuido (DVC) ###
Es un control de tal forma que varias personas pueden tener una copia del repositorio en su equipo y al hacer cambios se propaga a todos. En caso de caer un repositorio, se puede copiar otro y seguirá siendo lo mismo.

Fuentes: [http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)
[https://es.wikipedia.org/wiki/Control_de_versiones_distribuido](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

###c. Repositorio remoto y Repositorio local ###
Repositorio remoto son versiones del proyecto en el que haremos cambios locales, normalmente este repositorio local está en nuestra computadora.
El repositorio remoto es el del servidor, no necesariamente cercano a nosotros.

Fuentes:[https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts/](https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts/)                                                             
[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

###d. Copia de trabajo / Working Copy###
Una copia de trabajo de es una réplica exacta de archivos de un repositorio a su sistema local. Se usa para trabajar sobre ellos y no afectar los archivos originales.

Fuentes: [https://www.osmosislatina.com/cvs_info/wcopy.htm](https://www.osmosislatina.com/cvs_info/wcopy.htm)
[https://tortoisesvn.net/docs/release/TortoiseSVN_es/tsvn-basics-svn.html](https://tortoisesvn.net/docs/release/TortoiseSVN_es/tsvn-basics-svn.html)

###e. Área de Preparación / Staging Area###
Es un espacio orientado a almacenar todos los archivos que queremos incluir en la próxima confirmación.

Fuentes: [https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area](https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area)

[https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva)
###f. Preparar Cambios / Stage Changes###
Es el momento en el que los archivos están listos para ser confirmados, aún pueden ser modificados.

Fuente: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
###g. Confirmar cambios / Commit Changes###
Es el momento en donde se confirman las modificaciones efectuadas.

Fuente: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

###h. Commit###
Es la acción que realiza un cambio en el proyecto y que es almacenado en el repositorio, para que el resto de personas puedan acceder a él.

Fuente: [https://pressroom.hostalia.com/white-papers/control-versiones](https://pressroom.hostalia.com/white-papers/control-versiones)
###i. clone###
Es un comando con el cual se puede copiar un repositorio público a nuestra máquina local haciendo una copia exacta de su último estado.

Fuente: [https://styde.net/clone-y-fork-con-git-y-github/](https://styde.net/clone-y-fork-con-git-y-github/)
###j. pull###
El comando pull permite descargar los cambios más recientes desde un servidor remoto a su repositorio local y a su vez verificar el código más reciente del repositorio.

Fuente: [http://git.github.io/git-reference/remotes/#pull](http://git.github.io/git-reference/remotes/#pull)
###k. push###
Este comando lo que hace es enviar los cambios de tu repositorio remoto, permitiendo compartir los cambios realizados.

Fuente: [http://git.github.io/git-reference/remotes/#push](http://git.github.io/git-reference/remotes/#push)
###l. fetch###
Es similar al pull ya que se descargan los cambios más recientes desde un servidor remoto a su repositorio local, pero manteniendo su repositorio tal como está.

Fuente: [http://git.github.io/git-reference/remotes/#fetch](http://git.github.io/git-reference/remotes/#fetch)
###m. merge###
Se utiliza para combinar los cambios realizados en diferentes copias del mismo archivo del repositorio de origen. Normalmente usado para trabajar en el mismo archivo pero en diferentes líneas de código. 

Fuente: [https://svn.apache.org/repos/asf/openoffice/ooo-site/trunk/content/docs/ddCVS_cvsglossary.html.ko](https://svn.apache.org/repos/asf/openoffice/ooo-site/trunk/content/docs/ddCVS_cvsglossary.html.ko)
###n. status###
Este comando entrega el estado del repositorio actual.

Fuente: [http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)
###o. log###
Sirve para ver el historial de cambios.

Fuente: [http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)
###p. checkout###
Es el proceso de obtener una copia del proyecto desde el repositorio. Si la copia que se desea no es la última, debe ser especificada, ya que por defecto se obtiene la última. 

Fuente: [https://pressroom.hostalia.com/white-papers/control-versiones](https://pressroom.hostalia.com/white-papers/control-versiones)
###q. Rama / Branch###
Es una copia del proyecto, bajo el control de versiones, pero de forma aislada, haciendo que los cambios realizados en esta rama no afecten al resto del proyecto y viceversa.

Fuente: [https://pressroom.hostalia.com/white-papers/control-versiones
](https://pressroom.hostalia.com/white-papers/control-versiones)
###r. Etiqueta / Tag###
Las etiquetas se utilizan para marcar puntos importantes en el proyecto a través de su historia. Normalmente utilizado para marcar versiones lanzadas.
 
Fuente:[https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)