﻿# Glosario

### a. Control de versiones (VC)
Es un sistema aplicado a archivos modificables. Consiste en manejar respaldos de versiones antiguas de un archivo, para así poder revertir cambios realizados sobre este en caso de necesitarse.

[Referencia: Control de versiones VC][1]
### b. Control de versiones distribuido (DVC)
En este sistema de VC, los datos del repositorio alojados en el servidor son replicados por completo en todos los computadores que colaboran a través de él, y con esto, se mantienen copias de seguridad en caso de que el servidor falle.

[Referencia: Control de versiones distribuídas DVC][2]
### c. Repositorio remoto y Repositorio local
El repositorio local es aquel cuyos datos se encuentran almacenados en el ordenador desde donde se trabaja, en cambio, el repositorio remoto es aquel cuyos datos se encuentran alojados en un servidor 

[Referencia: Repositorio local][3.1]
[Referencia: Repositorio remoto][3.2]
### d. Copia de trabajo / Working Copy
Una copia de trabajo local es un repositorio descargado ("clon").  git al proporsionar un DVC, genera backups (distribuídos entre los colaboradores mediante el comando clon) y cualquier persona con un clon puede trabajar en él, organizando cuando y quien hizo los cambios al repositorio. 

[Referencia: Copia de trabajo][4]
### e. Área de Preparación / Staging Area
El área de preparacion es un archivo (Index) en el que se listan los archivos que han sido modificados pero que no se desea que se integren al repositorio de forma inmediata, sino que vienen a formar parte en la próxima confirmacion (aunque también pueden ser descartados).

[Referencia: Área de Preparación][5]
### f. Preparar Cambios /Stage Changes
Para preparar los cambios, basta con ejecutar el comando 

**git add *MiArchivo.extension*** .


----------


Como el archivo se alojará en el Área de preparación, deberá ser confirmado para volverse "permamente" en el repositorio local.

[Referencia: Preparar Cambios][6]
### g. Confirmar cambios / ​Commit Changes
Para el ejemplo anterior, tras haber preparado los cambios, se pueden confirmar mediante el comando 

**git commit -m *"Cambios en MiArchivo"*** .

Cabe destacar que para varios archivos en el área de preparación se pueden confirmar algunos (ya sea en grupos o por separado) en commits distintos. Así mismo, si se omite el "-m" en la instruccion anterior, se puede describir el cambio realizado desde un editor.

[Referencia: Confirmar cambios][7.1]

[Referencia: Omisión de -m en un commit][7.2]
### h. Commit
Se utiliza para confirmar archivos en un repositorio local (tal como se mencionó en el punto anterior).
[Referencia: Commit][8]
### i. Clone
Este comando se utiliza para copiar casi todos los datos de un repositorio remoto en el repositorio local.

[Referencia: Clone][9]
### j. Pull
se utiliza para recibir datos de un repositorio remoto. Con este comando se recupera y une automáticamente la rama remoto con la rama actual (con la que se está trabajando). Por lo general se recupera la información del servidor clonado, y automáticamente se intenta unir con el código que se está trabajando.  

[Referencia: Pull][10]

### k. Push
cuando tu proyecto se encuentra en un estado que quieres compartir, tienes que enviarlo a un repositorio remoto. Este comando funciona unicamente si has clonado el repositorio en el que tiene permiso de escritura. 

[Referencia: Push][11]
### l. Fetch
Fetch se utiliza para recuperar datos de tus repositorios remotos. Permite obtener los datos del proyecto que aún no tengas. Es importante tener en cuenta que el comando fetch sólo recupera la información y la pone en tu repositorio local, no la une automáticamente con tu trabajo ni modifica aquello en lo que estás trabajando, sino que tendrás que unir ambos de forma manual posteriormente.

[Referencia: Fetch][12]
### m. Merge
Merge se usa comunmente para unir dos ramas de trabajo.La rama actual se actualizará para reflejar la fusión, pero la rama de destino no se verá afectada por completo, este comando nos permite unir las lineas independientes creadas con ***git branch*** y fusionarlas en una sola rama.  

[Referencia: Merge][13]

### n. Status
Permite ver en el estado que se encuentran los archivos del repositorio (bajo seguimiento , modificados, directorio de trabajo limpio), también este comando nos dice en que rama de trabajo nos encontramos. El comando ***git status*** es importante al momento de preparar nuestros archivos para ponerlos en seguimiento y posteriormente confirmarlos.

[Referencia : status][14]
### o. Log
Después de hacer varias confirmaciones, el comando ***git log*** nos permite ver el historial de todas las confirmaciones que se hayan hecho al repositorio. Este comando nos da múltiples opciones, como ver las últimas dos entradas de cada repositorio , mostrar los cambios en orden cronológico o mostrar las diferencias introducidas en cada confirmación como uno de sus usos mas útiles. 

[Referencia: Log][15]
### p. Checkout
se utiliza para crear nuevas ramas y moverse a tráves de ellas, al utilizar ***git checkout*** creas una rama y quedas de forma activa en ella (checkout) ó al saltar a otra rama mediante ***git checkout -b [nombrerama]*** la cual se convertirá en tu rama activa. Esto nos permite movernos por las distintas ramas modificando los archivos y actualizándolos para luego (si es requerido) utilizar ***merge*** para fusionar las ramas creadas. 
 
 [Referencia: checkout][16]
### q. Rama / Branch
Git almacena los archivos como una serie de copias puntuales de los archivos completos, tal y como se encuentran en ese momento, al ir confirmando los cambios Git los ira guardando como objetos del arbol del repositorio, creando los datos pertinentes y un apuntador al árbol raíz. Por ende una rama es simplemente un apuntador móvil que apunta a cada confirmaciones que se vaya haciendo.
  La rama por defecto de Git es la rama master. Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación. En cada confirmación de cambios que realicemos, la rama irá avanzando automáticamente. Y la rama master apuntará siempre a la última confirmación realizada.  
  [Referencia: rama][17]
### r. Etiqueta / Tag
Las etiquetas son puntos especificos en el historial del repositorio, generalmente se utilizan para indicar cuando se haya lanzado alguna versión del proyecto. Se utilizan principalmente dos tipos de etiquetas, las ligeras que son como una rama que no cambia, es decir, un puntero a una confirmación específica, y las etiquetas anotadas, las cuales son almacenadas como objetos completos en la base de datos de Git. Tienen suma de comprobación; contienen el nombre del etiquetador, correo electrónico y fecha; tienen mensaje de etiquetado; y pueden estar firmadas y verificadas con GNU Privacy Guard (GPG). El uso de etiquetas nos permite saber cuando un proyecto posee una versión ya estable o terminada para ir actualizandola en el futuro.  
   [Referencia: etiqueta][18]

 [1]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
 [2]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones 
  [3.1]:https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/
  [3.2]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
  [4]:https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms
  [5]:http://gitready.com/beginner/2009/01/18/the-staging-area.html
  [6]:https://githowto.com/staging_changes
  [7.1]:https://githowto.com/staging_and_committing 
  [7.2]: https://githowto.com/commiting_changes
  [8]:https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git
  [9]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git
  [10]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
  [11]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
  [12]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
  [13]:https://www.atlassian.com/git/tutorials/using-branches/git-merge
  [14]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
  [15]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones
  [16]:https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
  [17]:https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F
  [18]:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
