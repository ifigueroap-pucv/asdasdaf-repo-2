# Glosario
**Control de versiones (VC):**
El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante. [Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**Control de versiones distribuido (DVC)**:
El control de versiones distribuido permite a muchos desarrolladores de software trabajar en un proyecto com�n sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido [Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**Repositorio remoto y local**:
Los repositorios remotos son versiones de tu proyecto que est�n hospedadas en Internet o en cualquier otra red. Mientras que los repositorios locales son versiones que se encuentran en el computador. [Git - Trabajar con remotos](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)

**Copia de trabajo / Working Copy**:
Una copia de trabajo es la informaci�n descargada desde un repositorio remoto al repositorio local al efectuar la clonaci�n. La descarga corresponde a la �ltima versi�n [Git - Obteniendo un repositorio Git](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)

**�rea de preparaci�n / Starting Area**:
El �rea de preparaci�n es un archivo, generalmente contenido en tu directorio de Git, que almacena informaci�n acerca de lo que va a ir en la pr�xima confirmaci�n. [Git - Fundamentos de Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**Preparar Cambios / Stage Changes**:
Preparar Cambios es el paso en que los archivos se a�aden con el comando add en el repositorio local, preparandose para a�adirse al repositorio remoto.

**Confirmar Cambios / Commit Changes**:
Confirmar Cambios es el paso en que una vez listos los archivos a a�adir o modificar (en que se aplica  add), confirmen los cambios al aplicar el comando commit. [Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**commit**:
El comando commit es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**clone**:
El comando clone se usa para crear una copia local del repositorio.  [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**pull**:
El comando pull es usado para poder fusionar todos los cambios que se han hecho en el repositorio local. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**push**:
Un push envia los cambios que se han hecho en la rama principal de los repertorios remotos que estan asociados con el directorio que esta trabajando. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**fetch**:
El comando fetch le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que esta trabajando. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**merge**:
El comando merge se usa para fusionar una rama con otra rama activa. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**status**:
El comando status muestra la lista de los archivos que se han cambiado junto con los archivos que estan por ser a�adidos o comprometidos. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**log**:
El comando log al ejecutarlo, muestra una lista de commits en una rama junto con todos los detalles. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**checkout**:
El comando checkout se puede usar para crear ramas o cambiar entre ellas. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**rama / branch**:
Las  **ramas**  son utilizadas para desarrollar funcionalidades aisladas unas de otras. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)

**etiqueta / tag**:
Generalmente se a�aden etiquetas para marcar puntos donde se ha lanzado alguna version. [Comandos basicos de GIT](https://www.hostinger.es/tutoriales/comandos-de-git)