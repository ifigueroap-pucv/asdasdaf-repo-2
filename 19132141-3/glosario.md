# Glosario

* Control de versiones (VC) : Sistema que permite mantener un historial de cambios de uno o varios archivos. Otorga acceso a cada versión de estos y permite retroceder los cambios con respecto a su estado actual. 
[Referencia]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones#Sistemas-de-control-de-versiones-locales
 

* Control de versiones distribuido (DVC): Sistema de control de versiones que permite descargar la última versión de un repositorio junto con todo su historial de cambios. De esta manera inhiben los efectos que podrían causar posibles problemas en el servidor.
[Referencia]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones#Sistemas-de-control-de-versiones-distribuidos 

* Repositorio remoto y Repositorio local: 
	* Repositorio local: espacio en donde se almacenen los cambios realizados por un usuario en un proyecto, previos a ser actualizados en el repositorio remoto en donde se encuentra el registro de las versiones de este.
	* Repositorio remoto: espacio (generalmente externo al ambiente de desarrollo) en donde se almacena el historial de versiones y cambios realizados por el/ los colaboradores.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

* Copia de trabajo / Working Copy: copia en local de un commit (versión) específico del repositorio.
[Referencia]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git#Los-tres-estados

* Área de Preparación / Staging Area: sección en el repositorio local en donde se almacena una lista de cambios realizados previos a ser confirmados en el próximo commit.
[Referencia]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git#Los-tres-estados

* Preparar Cambios / Stage Changes: acción de agregar cambios al área de preparación, se reliza con el comando add y es el paso previo (obligatorio) a relizar un commit.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Salt%C3%A1ndote-el-%C3%A1rea-de-preparaci%C3%B3n

* Confirmar cambios / Commit Changes: acción de confirmar los cambios agregador al área de preparación, estos cambios serán actualizados en el repositorio local.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios

* Commit: comando git que permite guardar cambios en nuestro repositorio local. 
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios

* clone: comando git que permite descargar en local un repositorio remoto con su historial de versiones completo y dejando ambos repositorio sincronizado.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git#Inicializando-un-repositorio-en-un-directorio-existente

* pull: comando git utilizado para sincronizar nuestro repositorio local con el remoto, actualizando posibles cambios realizados por otro colaboradores. 
[Referencia]: https://git-scm.com/docs/git-pull

* push: comando git que permite actualizar los cambios realizados en local (enviando los commits realizados) al repositorio remoto. Para ejecutar todos los cambios deben encontrarse agregados en el repositorio local.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos#Enviando-a-tus-repositorios-remotos

* fetch: comando git para actualizar cambios del repositorio remoto al local, actualizando además todos los cambios realizados en otras ramas de desarrollo (cuando no se indica una rama especifica). Este comando a diferencia del comando pull sólo actualiza los cambios realizados en remoto pero no une estos cambios con los que pudiesen existir en local, esta operación se debe realizar de forma manual.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos#Recibiendo-de-tus-repositorios-remotos

* merge: sincronización/acoplamiento de cambios (commits) en remoto y en local.
[Referencia]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar#Procedimientos-b%C3%A1sicos-de-fusi%C3%B3n

* status: comando git que permite ver el estado en que se ecuentra los archivos de un repositorio. 
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Comprobando-el-estado-de-tus-archivos

* log: comando que permite ver el historial de commits realizados en un repositorio.
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones

* checkout: comando utilizado para cambiar de rama dentro de un repositorio, indicando el nombre de la rama a la cual se desea cambiar.
[Referencia]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

* Rama / Branch: área de trabajo en donde se actulizan los cambios realizados en un repositorio. Por defecto todo repositorio posee una rama Master, pero es posible crear más ramas a partir de ella cuando es necesario trabajar de forma colaborativa.
[Referencia]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

* Etiqueta / Tag: permite identificar con un nombre especial commits críticos dentro de un repositorio, para luego facilitar su búsqueda. 
[Referencia]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas#Creando-etiquetas


