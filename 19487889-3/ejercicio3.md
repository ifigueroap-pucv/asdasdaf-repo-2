1. Branch:
* Cantidad: 1
* Nombre: master

2. Tag:
* Cantidad: 0
* Nombre: No existe ningún tag.

3. Commits:
    1.
        * Nombre: Leandro
        * Mensaje: Agrega el archivo glosario.md al repositorio.
        * Código hash: b227a2035f9446423e7183b97347985108a260cb
    2.
        *  Nombre: Emiliano Martinez Burgos
        *  Mensaje: Agrega el archivo datos.md
        *  Código hash: 09aba333e73afff67f64a86c5b20c40ac42e37cf
    3.
        * Nombre Emiliano Martinez Burgos
        * Mensaje: Agrega el archivo ejercicio3
        Este cambio agrega el archivo ejercicio3.md
        * Código hash: 49627e358a632ac7292dd6b81466de13efda81df