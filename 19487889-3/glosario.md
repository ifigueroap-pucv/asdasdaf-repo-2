# Glosario

  1. Control de versiones: Consiste en un sistema que registra todos los cambios en un archivo o proyecto, con lo cual se puede recuperar una versión específica en cualquier momento.
Fuentes: [wikipedia](https://es.wikipedia.org/wiki/Control_de_versiones), [git-scm](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones), [hipertextual](https://hipertextual.com/archivo/2014/04/sistema-control-versiones/)
  2. Control de versiones distribuido: Es un sistema que permite a los desarrolladores trabajar en distintas copias locales del proyecto y luego integrar su trabajo en la rama principal.
Fuentes: [wikipedia](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido), [git-scm](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones), [lnds](http://www.lnds.net/blog/2010/07/control-de-versiones-distribuido.html)
  3. Repositorio remoto y repositorio local: Los repositorios remotos son lugares en internet donde se alojan versiones de un proyecto. Por su parte, un repositorio remoto es aquel que se encuentra en el computador que se está usando.
Fuentes: [git-scm](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos), [colaboratorio](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
  4. Área de preparación: Es una ubicación virtual en la que se guardan los archivos que se incluirán en el siguiente commit.
Fuentes: [Networkfaculty](https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area)
  5. Copia de trabajo: Corresponde a una copia de un repositorio obtenida mediante el comando clone.
Fuente: [git-scm](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
  6. Preparar cambios: Informa a GIT sobre los cambios realizados, pero no es permanente en el repositorio, sino que serán incluidos en el próximo commit. Además, los cambios pueden no ser incluidos en el commit usando git reset.
Fuente: [githowto](https://githowto.com/staging_changes)
  7. Confirmar cambios: Significa que los cambios realizados se guardarán en el repositorio.
Fuente: [quora](https://www.quora.com/What-is-the-meaning-of-commit-and-stage-in-git)
  8. Commit: Identifica los cambios realizados en el ambiente de trabajo.
Fuente: [stackoverflow](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)
  9. Clone: Obtiene una copia de un repositorio.
Fuente: [wikipedia](https://es.wikipedia.org/wiki/Git#%C3%93rdenes_b%C3%A1sicas)
  10. Pull: Utiliza fetch para obtener los cambios de un repositorio y luego merge para unirlos a la rama actual.
Fuentes: [stackoverflow](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git), [wikipedia](https://es.wikipedia.org/wiki/Git#%C3%93rdenes_b%C3%A1sicas)
  11. Push: Carga los cambios realizados en una rama de trabajo a otra.
Fuente: [stackoverflow](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)
  12. Fetch: Descarga los cambios de un repositorio remoto y los guarda en la rama origin/master.
Fuente: [stackoverflow](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git), [wikipedia](https://es.wikipedia.org/wiki/Git#%C3%93rdenes_b%C3%A1sicas)
  13. Merge: Combina los cambios de la rama indicada con la rama actual.
Fuente: [stackoverflow](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git), [wikipedia](https://es.wikipedia.org/wiki/Git#%C3%93rdenes_b%C3%A1sicas)
  14. Status: Muestra el estado local actual del repositorio.
Fuente: [loquemeinteresadelared](https://loquemeinteresadelared.wordpress.com/tag/git-status/)
  15. Log: Sirve para listar los cambios realizados en el proyecto. Es decir, sirve para "mirar atrás en el tiempo".
Fuente: [git-scm](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)
  16. Checkout: Sirve para crear una rama nueva o moverse de una a otra.
Fuente: [hostinger](https://www.hostinger.es/tutoriales/comandos-de-git)
  17. Rama / branch: Una rama es un apunta a una de las confirmaciones que se realizan sobre el repositorio. El comando branch sirve para listar, crear y eliminar ramas según el argumento utilizado.
Fuentes: [git-scm](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell), [hostinger](https://www.hostinger.es/tutoriales/comandos-de-git)
  18. Etiqueta / tag: No es más que una cadena de caracteres que apunta a un punto específico en la historia del proyecto, es decir, apunta a un commit específico, por lo que se mantiene constante.
Fuentes: [genbetadev](https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git), [git-scm](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)