﻿# Glosario Git
1. [Control de versiones (VC)](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Empezando Acerca del control de versiones"): 
Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que se pueda recuperar versiones específicas más adelante.

2. [Control de versiones distribuido (DVC)](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Empezando Acerca del control de versiones"):	
En un DVCS, ademas de descargar la última instantánea de los archivos, los clientes replican completamente el repositorio.

3. [Repositorio remoto y Repositorio local](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Trabajando con repositorios remotos"):	
Los repositorios remotos son versiones del proyecto que se encuentran alojados en Internet o en algún punto de la red. Mientras que los repositorios locales son las versiones alojadas en el equipo o computador local.

4. [Copia de trabajo / Working Copy](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms "Git Basic Terms"):
La copia de trabajo local de un usuario (un "clon" del comando "git clone") representa un repositorio completo localizado en la red.

5. [Área de Preparación / Staging Area](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics "Getting Started Git Basics"): 
El área de preparación el un archivo, generalmente contenido en el directorio de git del usuario, que almacena la información acerca de que ira en el próximo "commit". También es llamado como "index" o indice.

6. [Preparar Cambios / Stage Changes](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git "Empezando Fundamentos de Git"): 
Significa que git sabe acerca del cambio, pero este no es permanente en el repositorio. En el siguiente commit se incluirán los cambios que están preparados.

7. [Confirmar cambios / Commit Changes](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio "Guardando cambios en el repositorio"): 
Se hacen cambios, se preparan los archivos modificados y se confirman instantáneas de estos al repositorio remoto cada vez que el proyecto alcance un estado que se desee grabar.

8. [Commit](https://git-scm.com/docs/git-commit "Google's Homepage"): 
Almacena el estado actual de los contenidos del indice en un nuevo "commit" junto con un mensaje de log del usuario describiendo los cambios.

9. [clone](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git "Google's Homepage"): 
Comando que sirve para obtener una copia de un repositorio Git existente.

10. [pull](/git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Google's Homepage"): 
Comando que sirve para recuperar la información del servidor del que clonaste, y automáticamente se intenta unir con el código con el que estás trabajando actualmente.

11. [push](https://help.github.com/articles/pushing-to-a-remote/ "Pushing to a remote"): 
Comando que guarda los cambios locales al repositorio online.

12. [fetch](https://git-scm.com/docs/git-fetch "Git fetch"): 
extrae ramas y/o etiquetas (llamadas en conjunto "referencias") de uno o mas repositorios, junto con los objetos necesarios para completar sus historias.

13. [merge](https://git-scm.com/docs/git-merge "Git merge"): 
Unir dos o mas historias de desarrollo juntas.

14. [status](https://git-scm.com/docs/git-status "Git status"): 
Muestra caminos que tienen diferencias entre el archivo de indice y el head commit actual, caminos que tienen diferencias entre el árbol de trabajo y el archivo indice, y caminos en el árbol de trabajo que no tienen seguimiento por Git.

15. [log](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones "Viendo el histórico de confirmaciones"): 
 Sirve para mirar atrás para ver qué modificaciones se han llevado a cabo.

16. [checkout](https://git-scm.com/docs/git-checkout "Git checkout"):
actualiza los archivos en el arbol de trabajo para igualar la version del indice o de un arbol especificado.

17. [Rama / Branch](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms "Git basic terms"):	
Las ramas son utilizadas para crear arboles de trabajo separados. tambien pueden ser vistas como un nuevo contexto de desarrollo.

18. [Etiqueta / Tag](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas "Git creando etiquetas"): 
Puntos específicos en la historia marcados como importantes. Generalmente la gente usa esta funcionalidad para marcar puntos donde se ha lanzado alguna versión (v1.0, y así sucesivamente).
