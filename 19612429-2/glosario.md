#Glosario de Términos
1. **Control de Versiones (VC):**
	El control de versiones es un sistema que tiene como objetivo registrar los cambios realizados sobre un archivo o conjunto de estos a lo largo del tiempo, dando con ello la posibilidad de recuperar versiones anteriores específicas si se considera necesario. A pesar de que este se aplique principalmente a código fuente, cualquier tipo de archivo en un ordenador puede estar bajo un control de versiones. El término versión aplicado en este concepto hace referencia al estado del elemento en un momento dado de su desarrollo o modificación.

    *Fuente: [Git - Acerca del Control de Versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones), [Wikipedia - Control de Versiones](https://es.wikipedia.org/wiki/Control_de_versiones)*

2. **Control de Versiones Distribuido (DVC):**
	El control de versiones distribuido cumple la tarea de registrar los cambios realizados sobre un archivo o conjunto de estos a lo largo de tiempo, permitiendo recuperar versiones anteriores si es necesario. Se caracteriza principalmente por permitir que varias personas en distintos grupos puedan colaborar dentro de un mismo proyecto sin necesidad de compartir la misma red. Este enfoque otorga la posibilidad de establecer varios flujos de trabajo; como pueden ser los modelos jerárquicos, los cuales en sistemas centralizados no pueden ser aplicados. El DVC no posee un único repositorio central donde los clientes se sincronizan, sino que cada uno posee una copia completa de este. Por lo tanto, permite tener una copia de seguridad de todos los datos en caso de un fallo.

    *Fuente: [Git - Sistemas de control de versiones distribuidos](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones#Sistemas-de-control-de-versiones-distribuidos), [Wikipedia - Control de versiones distribuido](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)*

3. **Repositorio Remoto y Repositorio Local:**
	Un repositorio se define como un espacio que se utiliza para almacenar algún elemento físico o simbólico, en el caso de la informática estos guardan datos, archivos, versiones de un proyecto, etc. Se caracterizan por poseer sistemas de respaldo y mantenimiento preventivo y correctivo, lo que permite recuperar la información en caso de que esta pudiera dañarse. Un repositorio remoto contiene las versiones, documentos o elementos de un proyecto, pero están alojados en Internet o en algún punto de la red. Pueden existir varios y cada cual tendrá permisos de escritura o lectura distintos. Por otra parte, uno local contiene la misma información de uno remoto, pero se encuentra alojado en la computadora desde la cual se está trabajando o haciendo referencia.

    *Fuente: [Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos), [Definición - Repositorio](https://definicion.de/repositorio/), [Wikipedia - Repositorio](https://es.wikipedia.org/wiki/Repositorio), [Git's local repository and remote repository — confusing concepts](https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts)*

4. **Copia de Trabajo:**
	Los archivos de un repositorio no pueden ser modificados directamente, por esta razón se debe obtener una copia de estos para poder trabajarlos. Esta copia que se encuentra alojada en el computador es denominada Copia de Trabajo y está vinculada con la localización original en el repositorio. Este vínculo es persistente y por lo general no cambia durante la vida dicha copia. Por lo tanto, es automáticamente referenciada durante sucesivas operaciones de actualización.

    *Fuente: [Assembla - Working Copies](https://cornerstone.assembla.com/cornerstone/helpbook/pages/introduction/terminology/working-copies.html)*

5. **Área de Preparación:**
	El área de preparación, también denominado index, es una de las tres zonas que en Git existen para los datos. En particular, esta se encarga de almacenar los cambios que se han realizado sobre archivos y gestionar cuáles de ellos serán efectivamente aplicados al repositorio. Esta capa intermedia entre la copia de trabajo y el repositorio, permite una gran flexibilidad y control sobre los cambios que se envíen, ya que los desarrolladores pueden administrarlos como ellos deseen.

    *Fuente: [Git Ready - the Staging Area](http://gitready.com/beginner/2009/01/18/the-staging-area.html)*

6. **Preparar Cambios:**
	Es el proceso mediante el cual las modificaciones hechas sobre un archivo en el área de trabajo son llevadas al área de preparación. Esto quiere decir, que los cambios realizados sobre un fichero son llevados a un estado preparado, en el cual están a la espera de confirmación para ser aplicados al repositorio.

    *Fuente: [GitHowTo - Staging the Changes](https://githowto.com/staging_changes), [StackExchange - What does 'stage' mean in git?](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git), [Git - Preparando archivos modificados](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Preparando-archivos-modificados)*

7. **Confirmar Cambios:**
	Es el proceso en cual todos los cambios que se encuentran en el área de preparación son aplicados al repositorio. Al confirmar los cambios, cualquier modificación que no haya sido preparada no será incluida. Este proceso al finalizar deja información acerca de la rama afectada, cantidad de archivos modificados, etc. Además, cada vez que una confirmación de cambios es realizada, se genera una instantánea del proyecto, a la que se puede acceder para restaurar o comparar.

    *Fuente: [Git - Confirmando tus cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios)*

8. **Commit:**
	Comando de Git el cual aplica los cambios que se encuentran en el área de preparación (index) al repositorio, junto con ello agrega un mensaje escrito por el usuario el cual describe las modificaciones realizadas. Posee distintos parámetros los cuales modifican la forma en la cual se ejecuta, entre estos, realizar un commit permitiendo un mensaje del usuario vacío, mostrar información de la rama afectada o información de seguimiento, etc.

    *Fuente: [Git - Commit](https://git-scm.com/docs/git-commit)*

9. **Clone:**
	Comando de Git el cual clona un repositorio dentro de un nuevo directorio, junto con esto se crean ramas de seguimiento remoto para cada una de las que existe en el repositorio original. Además, se crea una rama inicial que es una copia de la que se encuentra activa en el repositorio que se está clonando. Al finalizar, este comando se asegura que las ramas remotas estén actualizadas y la rama maestra no tenga diferencia, a través de los comandos pull y fetch.

    *Fuente: [Git - Clone](https://git-scm.com/docs/git-clone)*

10. **Pull:**
	Comando de Git el cual incorpora cambios desde un repositorio remoto a la rama en la que se está trabajando. Este no solo descarga los datos nuevos, sino que los integra en las copias de trabajo de los archivos. Internamente el comando ejecuta un fetch para obtener los cambios y un merge para integrarlos.

    *Fuente: [Git - Pull](https://git-scm.com/docs/git-pull), [Git-Tower - What's the difference between git fetch and git pull?](https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull)*

11. **Push:**
	Comando de Git el cual es usado para actualizar un repositorio remoto con los cambios que se han realizado en uno local. Existen dos actores en este comando, el primero es la fuente (la rama de la cual los datos deben ser subidos), la cual siempre corresponde a la rama actual. Mientras que el objetivo corresponde a la rama a la cual se deben subir los datos, este puede ser especificado u omitido si es que existe un seguimiento remoto.

    *Fuente: [Git-Tower - Git Push](https://www.git-tower.com/learn/git/commands/git-push), [Git - Push](https://git-scm.com/docs/git-push)*

12. **Fetch:**
	Comando de Git el cual obtiene nuevos cambios que existen en el repositorio remoto, pero no integra nada de esta nueva información a los archivos en los que se esté trabajando. Este comando es útil para tener una visión actualizada de lo que ha pasado en dicho repositorio. Además, se puede obtener el contenido desde uno o varios repositorios si es que en los parámetros se es indicado.

    *Fuente: [Git - Fetch](https://git-scm.com/docs/git-fetch), [Git-Tower - What's the difference between git fetch and git pull?](https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull)*

13. **Merge:**
	Comando de Git el cual integra en una única rama líneas de desarrollo que en algún punto se dividieron, permitiendo con ello combinar múltiples secuencias de commits en un único conjunto de estos. Cuando se realiza un merge la rama en la que se esté ubicado (rama actual) se actualizará reflejando la fusión entre ambas, pero la otra rama (rama objetivo) no sufrirá cambio alguno.

    *Fuente: [Git - Merge](https://git-scm.com/docs/git-merge), [Atlassian - Git Merge](https://www.atlassian.com/git/tutorials/using-branches/git-merge)*

14. **Status:**
	Comando de Git el cual muestra el estado del directorio de trabajo y del área de preparación. Este permite ver los cambios que están preparados para el próximo commit, los que no lo están y los archivos que no están bajo seguimiento por Git. Además, otorga una visión actualizada sobre los cambios en los archivos, dando con ello un mayor control y evitando que se realicen commits no deseados o con falta de modificaciones.

    *Fuente: [Git - Status](https://git-scm.com/docs/git-status), [Atlassian - Inspecting a repository, Git Status](https://www.atlassian.com/git/tutorials/inspecting-a-repository#git-status)*

15. **Log:**
	Comando de Git el cual muestra el detalle de todos los commits realizados. Permite listar la historia del proyecto, filtrarla y buscar por cambios o puntos específicos de este. El formato de salida del comando puede ser personalizado debido a los diferentes parámetros que pueden agregarse, dando lugar a una forma completamente editable del detalle obtenido.

    *Fuente: [Git - Log](https://git-scm.com/docs/git-log), [Atlassian - Inspecting a repository, Git Log](https://www.atlassian.com/git/tutorials/inspecting-a-repository#git-log)*

16. **Checkout:**
	Comando de Git el cual se define como el acto de cambiar entre diferentes versiones de una entidad objetivo. Este trabaja con tres tipos de entidades particulares, los archivos, las ramas y los commits. Esto permite la navegación entre distintas ramas, lo cual puede entenderse como el desplazamiento desde una línea de desarrollo particular a otra en la que se desea trabajar. Además, a través de la navegación hacia los commits se puede volver a un estado específico del proyecto.

    *Fuente: [Git - Checkout](https://git-scm.com/docs/git-checkout), [Atlassian - Git Checkout](https://www.atlassian.com/git/tutorials/using-branches/git-checkout)*

17. **Rama:**
	Una rama en Git es un puntero desplazable que se encuentra apuntando a un commit particular, es decir, está direccionado a una versión específica del proyecto. Se pueden representar como líneas independientes de desarrollo, ya que cada una apunta a una instantánea particular, la cual contiene toda la información con los cambios que en esta existían. Además, a diferencia de otros sistemas de control de versiones, la creación y destrucción de ramas en Git es muy simple y rápida, ya que solo contienen la información de la confirmación de cambios a la que apuntan.

    *Fuente: [Git - ¿Qué es una rama?](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F), [Atlassian - Git Branch](https://www.atlassian.com/git/tutorials/using-branches)*

18. **Etiqueta:**
	Una etiqueta en Git es un puntero a un commit específico. Son similares a una rama que no cambia de posición, es decir, son referencias inmutables. Existen dos tipos principales de etiquetas: las ligeras y las anotadas. La primera de estas, simplemente apunta a un commit en particular sin ningún tipo de información extra. Mientras que la segunda, crea un objeto adicional que es guardado en el repositorio de Git, el cual contiene información asociada al tag como: nombre del etiquetador, fecha, correo electrónico, un mensaje asociado, firmas, etc.

	*Fuente: [Git - Etiquetado](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado), [AlBlue's Blog - Git Tip of the Week: Tags](http://alblue.bandlem.com/2011/04/git-tip-of-week-tags.html)*
