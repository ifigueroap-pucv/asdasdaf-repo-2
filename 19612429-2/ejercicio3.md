#Ejercicio 3

1. **Cantidad y nombre de Branches:**

	**Cantidad:** 1

    **Nombre:** master

    A la fecha tan solo existe la rama master

2. **Cantidad y nombre de las Etiquetas:**

	**Cantidad:** 0

    **Nombre:**

    A la fecha no existe ninguna etiqueta

3. **Nombre, mensaje y código Hash de los últimos 3 commits:**

	Nombre | Mensaje | Código Hash
    --- | --- | ---
    Bryan Cataldo | Agrega archivo glosario.md | 47ce848021e34222a8698e90628441a4782f1373
    Luis Felipe Gutierrez Espindola | Correción en archivo ejercicio3.md | fed10d0a82e1efb56323862e5e4b55780cbdbaea
    Luis Felipe Gutierrez Espindola | Correción ortográfica en glosario.md | cba0a3eda49272e34e535b88e4a16abd6afc00c6
