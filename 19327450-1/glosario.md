Control de versiones 

: Sistema que registra cada cambio realizado a un archivo o proyecto, a medida que se edita, de manera que se puede recuperar cualquier versi�n anterior de ser necesario.  
Fuente: <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

Control de versiones distribuido 

: Sistema que permite mantener una copia completa del repositorio que utilizamos al momento de trabajar, manteniendo un respaldo completo en caso de un error en el repositorio original.  
Fuente: <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

Repositorio remoto 

: Versiones de un proyecto propio o de tu grupo de trabajo almacenado en un servidor o en la web, que permite que varios usuarios accedan a �l de manera remota.  
Fuente: <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

Repositorio local 

: Almacenamiento de versiones anteriores de un archivo o proyecto, guardado en un archivo, para respaldo.  
Fuente: <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git>

Copia de trabajo 

: Respaldo de un archivo o proyecto en caso de que ocurra alg�n error.  
Fuente: <https://neliosoftware.com/es/blog/copias-de-seguridad-con-git/>

�rea de preparaci�n 

: Capa intermedia entre el �rea de trabajo y el respaldo de Git en la que se decide que cambios se guardaran en el repositorio y cuales no.  
Fuente: <http://gitready.com/beginner/2009/01/18/the-staging-area.html>

Preparar cambios 

:

Confirmar cambios 

:

Commit 

: Almacena el contenido actual del index como un cambio a realizar, junto con los datos del usuario que realiz� el cambio.  
Fuente: <https://git-scm.com/docs/git-commit>

clone 

: Copia un repositorio existente en el repositorio actual.  
Fuente: <https://git-scm.com/docs/git-clone>

pull 

: Recupera los cambios realizados a un repositorio remoto.  
Fuente: <https://git-scm.com/docs/git-pull>

push 

: actualiza los cambios realizados al repositorio remoto con el cu�l se est� trabajando.  
Fuente <https://git-scm.com/docs/git-push>

fetch 

: Obtiene ramas y etiquetas de uno o m�s repositorios junto con los archivos que sean necesarios.  
Fuente: <https://git-scm.com/docs/git-fetch>

merge 

: Incorpora cambios realizados en una rama especificada, a la rama actual.  
Fuente: <https://git-scm.com/docs/git-merge>

status 

: Muestra el estado del arbol de trabajo.  
Fuente: <https://git-scm.com/docs/git-status>

log 

: Muestra los registros de confirmacion.  
Fuente: <https://git-scm.com/docs/git-log>

checkout 

: Actualiza los archivos en el �rbol de trabajo para que coincidan con la versi�n en el �ndice o el �rbol especificado.  
Fuente: <https://git-scm.com/docs/git-checkout>

Rama 

: Apuntador movil que guarda un aconfirmacion de cambio.  
Fuente: <https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F>

Etiqueta 

: Puntero a un cambio realizado que contiene la informaci�n del creador de la etiqueta y puede contener informaci�n sobre este cambio marcado.  
Fuente: <https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado>