﻿# Glosario

Alumno: Roberto Suárez Órdenes
Ramo: Ingeniería Web
Profesor: Ismael Figueroa


# Términos



## a. Control de versiones (VC)

Sistema en el cual se puede ir almacenando el progreso del proyecto, con respaldo y así no perder el trabajo realizado hasta entonces, desde un servidor central, para tenerlo de manera ordenada y poder monitorearlo.
**https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones**


## b. Control de versiones distribuido (DVC)

La diferencia con el anterior, este no posee un servidor central para conseguir otras versiones del proyecto, mas bien, es el programador el que hace uso de copias en los dispositivos que este tenga.
**https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones**
## c. Repositorio remoto y Repositorio local

Repositorio sería la carpeta donde se trabaja el proyecto, dícese remoto cuando se habla del que está almacenado en la nube (github, bitbucket por ejemplo). Hablando del local, es de donde se trabaja. como el computador personal, estos repositorios deben de estar enlazados con el init y posterior clone, y un logueo a dichas cuentas para sincronizar.

**https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos**


----------


## d. Copia de trabajo / Working Copy

Es un "piso" de trabajo, para que cuando ocurra algo con los archivos, tener de respaldo un proyecto funcional y sin fallos.

**http://brettterpstra.com/2015/05/21/working-copy-git-productivity-on-ios-sponsor/**

## e. Área de Preparación / Staging Area
Es un sitio web privado que sirve como una previsualización del trabajo hecho por los desarrolloradores.

**http://www.commonplaces.com/blog/web-development-what-is-staging/**

## f. Preparar cambios / Stage Changes
Prepara los cambios ya hechos y los añade al staging area.

**https://git-scm.com/book/es/v2**
## g. Confirmar cambios / Commit Changes
Confirma los cambios en el punto anterior.
**https://git-scm.com/book/es/v2**

## h. Commit

Comenta los cambios (de preferencia) luego del add, si se está vinculado con github o bitbucket, estos comentarios se ven reflejados en los repositorios de dichas páginas.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## i. clone

Descarga todo el repositorio remoto al local, se hace generalmente luego del init.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## j. pull

Descarga solo los cambios que están en el repositorio remoto al local.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## k. push

Subir los cambios desde el repositorio local al remoto, ya haciendo los commits.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## l. fetch
Busca todos los archivos del repositorio remoto que no están actualmente en el repositorio local.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## m. merge

Se usa para fusionar ramas, la activa, y la seleccionada.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## n. status
Muestra una lista de los archivos que están comprometidos hasta el momento (no se hayan subido, sus cambios).
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**

## o. log
Te entrega un listado de los últimos cambios junto con sus commits y desde donde se suben (nombre del contribuidor).
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## p. checkout
Se usa para navegar entre las ramas del proyecto.

**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**
## q. Rama / Branch
Para listar, crear o borrar ramas del repositorio.

**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**

## r. Etiqueta / Tag

Sirve para remarcar commits, importantes de preferencia.
**[https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)**


