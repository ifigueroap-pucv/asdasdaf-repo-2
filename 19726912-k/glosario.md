﻿# Glosario
## Andrés Guerrero Herrera
### 19.726.912-K	    Ingeniería Civil Informática

**a. Control de versiones (VC)**:  es un sistema que guarda los cambios realizados sobre un archivo o conjunto de archivos con fechas específicas, en caso de error en cierto tramo, se puede recuperar el proyecto en la fecha en la cual funcionaba. Se considera como una fotografía.
#####  [Fuente de información sobre DVC](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**b. Control de versiones distribuido (DVC)**: Contiene un servidor central, pero cada usuario contiene clonaciones de este. Permite que se pueda trabajar de forma múltiple un proyecto de Software, sin ser obligatorio estar conectados en la misma red. 
No depende de un solo servidor físico.
##### [Fuente de información sobre DVC](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

**c. Repositorio remoto y Repositorio local:** Los *Repositorio remoto* son versiones del proyecto que se encuentran anclados en la red. Cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas. Colaborar con otros implica gestionar estos repositorios. Se conocen como los push de los repositorios locales.
Los *Repositorios Locales* son versiones del proyecto que están guardadas en el computador . 
##### [Fuente de información sobre Repositorio Remoto y Local](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

**d. Copia de trabajo / Working Copy:** Se crea mediante Check Out, y es un área local que contiene una copia de  una colección de archivos, los cuales se pueden editar según prefiera y si se trata de archivos de código fuente, se puede compilar a partir de ellos. Es un área  privada, no visible, al estar seguro se puede compartir con otros usuarios y mezclar. 
##### [Fuente de información sobre Copia de Trabajo](http://svnbook.red-bean.com/es/1.0/svn-ch-2-sect-3.html)

**e. Área de Preparación / Staging Area:** También se le llama index. Es el lugar en el que se almacenan los cambios previos al commit. 
##### [Fuente de información sobre Stagin Area](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

**f. Preparar Cambios / Stage Changes:**  cualquier archivo que se haya creado o modificado, y sobre el cual se  ejecute el comando *git add* después de la su última edición, lo cual varia su estado y solo depende de un commit.
##### [Fuente de información sobre Stage Changes](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**g. Confirmar cambios / Commit Changes :**  Registra la "*foto*" del área de preparación, la cual está lista para ser consultada más adelante.
##### [Fuente de información sobre Commit Changes]( https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**h. commit:** Esto crea un nuevo objeto commit en el repositorio que identifica una nueva versión del contenido del repositorio. Esta revisión puede ser consultada posteriormente, por ejemplo si uno quiere ver el código fuente de una versión anterior. Posee información acerca del autor, la fecha y otros datos que nos pueden resultar prácticos a la hora de tratar de encontrar uno determinado. Y los cambios quedan en HEAD de la copia local. 
##### [Fuente 1 de información sobre Commit]( https://www.youtube.com/watch?v=QGKTdL7GG24)
##### [Fuente 2 de información sobre Commit]( http://rogerdudler.github.io/git-guide/index.es.html)

**i. clone:** Sirve para obtener una copia de un repositorio Git existente, para contribuir en algún proyecto, y recibe toda la información versionada del servidor.
##### [Fuente  de información sobre Clone]( https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)


**j. pull:** Comando que sirve para recuperar y unir automáticamente la rama remota con la rama actual. Al ejecutar este comando, por lo general se recupera la información del servidor del que se clonó y automáticamente, se intenta unir con el código con el que se está trabajando. Es usar git fetch+ git merge.
##### [Fuente  de información sobre Pull](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

**k. push:** Permite que un proyecto que se encuentra en un estado en que se desea compartir, se  envie a un repositorio remoto. El comando que permite hacer esto es “git push", el cual transfiere los cambios.
##### [Fuente  de información sobre push](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**l. fetch:** Es un comando que recupera todos los datos del proyecto remoto que no se tenga todavía. El comando fetch sólo recupera la información y la ubica en el repositorio local, no la une automáticamente, ni modifica aquello en lo que se está trabajando.
##### [Fuente  de información sobre fetch](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**m. merge:** se usa para fusionar uno o más ramas dentro de la rama que está activa. Luego avanza la rama actual al resultado de la fusión.
##### [Fuente  de información sobre Merge]( https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar)

**n. status:** Sirve para identificar si un archivo tiene seguimiento o no, es decir, si en el último commit un archivo no se encontraba, no tendrá seguimiento, a menos q se pida seguimiento, todo esto para evitar archivos autogenerados.
##### [Fuente  de información sobre Status](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**o. log:** Se utiliza para mostrar la historia  alcanzable de un proyecto desde la más reciente *foto* confirmada hacia atrás. Por defecto sólo se mostrará la historia de la rama actual. También se utiliza a menudo para mostrar las diferencias entre dos o más ramas a nivel de commit.
##### [Fuente  de información sobre Log]( https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar)

**p. checkout:** se usa para cambiar de rama y revisar el contenido del directorio de trabajo y para copiar archivos de los commits (o desde stage) , y para opcionalmente intercambiar branches.
##### [Fuente  de información sobre Checkout](https://marklodato.github.io/visual-git-guide/index-es.html)

**q. Rama / Branch :** se usan para desarrollar funcionalidades aisladas unas de otras y no están disponibles para todos, excepto que se haga un PUSH. Como comando es una gestión de ramas y  es un puntero con un nombre determinado por el usuario que apunta a un commit, La creación de un nuevo commit hace avanzar el puntero a esta nueva instancia. Cada commit conoce sus antecesores así como a sus sucesores en caso de tenerlos. Uno de los branches es el default, generalmente llamado master.
##### [Fuente  de información sobre Rama]( http://rogerdudler.github.io/git-guide/index.es.html)

**r. Etiqueta / Tag:** Es un marcador permanente a un punto específico, es decir, apunta a un commit que identifica una versión del repositorio. Con él, se puede tener un puntero con nombre al que siempre se podrá revertir los cambios. 
#####  [Fuente  de información sobre Tag](http://rogerdudler.github.io/git-guide/index.es.html)


----------


