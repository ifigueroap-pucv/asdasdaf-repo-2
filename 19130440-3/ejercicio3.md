1. **Cantidad y nombre de los branches:** 1 - "master"
2. **Cantidad y nombre de las etiquetas:** 0 - no existen tags
3. **Nombre, mensaje y código hash de los últimos 3 commits:**
	1. Luis Felipe Gutierrez Espindola - "_Fix a los hipervínculos en glosario.md_" - 9b7ebd5151530def67ebd02aa51f9fa19b08e2b0
	2. Luis Felipe Gutierrez Espindola - "_Corrección en archivo ejercicio3.md_" - fed10d0a82e1efb56323862e5e4b55780cbdbaea
	3. Luis Felipe Gutierrez Espindola - "_Corrección ortográfica en glosario.md_" - cba0a3eda49272e34e535b88e4a16abd6afc00c6
