﻿# Glosario Taller 0 Ing. Web
__a.	Control de versiones (VC):__	Sistema que registra los cambios realizados sobre un archivo o conjunto de ellos a lo largo del tiempo, para poder recuperar versiones específicas más adelante.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

__b.	Control de versiones distribuido (DVC):__	Sistema que permite a más de un desarrollador de software trabajar en un proyecto común sin la necesidad de compartir la misma red.
[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

__c.	Repositorio remoto y Repositorio Local:__	Un repositorio remoto es la versión de un proyecto que se encuentra alojado en internet, por otro lado, un repositorio local es la versión del proyecto alojada en un computador.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

__d.	Copia de trabajo/Working Copy:__	Copia del repositorio remoto que permite a un desarrollador realizar modificaciones para luego retornar al servidor donde se encuentra el repositorio.
[Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms) 

__e.	Área de Preparación/Staging Area:__	Archivo que almacena información acerca de lo que será añadido al repositorio local en el próximo commit
[Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

__f.	Preparar Cambios/Stage Changes:__	Consiste en notificar a Git que hay archivos modificados, que luego serán incluidos en la próxima confirmación. 
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

__g.	Confirmar Cambios/Commit Changes:__	Consiste en aplicar al repositorio local, la o las modificaciones realizadas a los archivos.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

__h.	_Commit_:__	Comando que confirma en el repositorio local los cambios que han sido realizados en los archivos.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

__i.	_Clone_:__	Comando que permite obtener una copia de un repositorio Git existente.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

__j.	_Pull_:__	Comando que permite descargar una rama determinada del repositorio remoto y situarla en el repositorio local.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__k.	_Push_:__	Comando que permite enviar al repositorio remoto las confirmaciones realizadas en el repositorio local.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__l.	_Fetch_:__	Comando que permite buscar todos los archivos de un repositorio remoto que no residen en el repositorio local en que se está trabajando.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__m.	_Merge_:__	Comando que se usa para fusionar una rama con otra.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__n.	_Status_:__	Comando que muestra la lista de archivos modificados junto con los archivos que están preparados y confirmados. 
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__o.	_Log_:__	Comando que muestra la lista de commits realizados en una rama con sus detalles respectivos.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)

__p.	_Checkout_:__	Comando que permite crear ramas o cambiarse entre ellas.
[Fuente](https://www.hostinger.es/tutoriales/comandos-de-git)


__q.	_Rama/Branch_:__	 Es un puntero que apunta a alguna de las confirmaciones de cambios realizadas.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

__r.	_Etiqueta/Tag_:__	Punto específico en el historial de cambios del proyecto que es marcado como importante, como el lanzamiento de una nueva versión o actualización.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
