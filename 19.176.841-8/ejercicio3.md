﻿# Ejercicio 3

**a.	Cantidad y nombre de los branches:**	Solo un branch, de nombre "master"

**b.	Cantidad y nombre de etiquetas:** Hasta el momento no existen etiquetas presentes en el proyecto

**c.	Nombre, mensaje y código hash de los últimos tres _commits_:**	De momento, solo aparecen dos commit, los cuales son:

**_1° Commit_**
* Nombre:	Luis Felipe Gutierrez Espindola
* Mensaje:	Actualización ejercicio3.md
* Código Hash:	commit a0e9af67cb4e64d43bdbd8d00ee8e017086457e8

**_2° Commit_**
* Nombre:	Luis Felipe Gutierrez Espindola
* Mensaje:	Fix a los hipervínculos en glosario.md
* Código Hash:	commit 9b7ebd5151530def67ebd02aa51f9fa19b08e2b0

**_3° Commit_**
* Nombre:	Luis Felipe Gutierrez Espindola
* Mensaje:	Corrección en archivo ejercicio3.md
* Código Hash:	commit fed10d0a82e1efb56323862e5e4b55780cbdbaea

	

