﻿# Glosario:

- **Control de versiones (VC):**
 El control de versiones es una herramienta que permite el registro de los cambios realizados en un proyecto, de modo que sea posible acceder a todas sus fases de desarrollo a lo largo del tiempo. Si bien, bajo las circunstancias del curso se establece el código fuente de un programa como archivo bajo control de versiones, es posible que cualquier tipo de archivo pueda ponerse bajo un VC.
  [Git](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- **Control de versiones distribuido:** 
El DVCS (en Inglés) permite a varios desarrolladores de software trabajar en un proyecto común sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido.
De esta forma surge la ventaja de que si un servidor que contiene todos los datos falla, cualquier repositorio de los clientes puede copiarlo nuevamente en el servidor para su restauración. 
[Lnds](http://www.lnds.net/blog/2010/07/control-de-versiones-distribuido.html)
[Git](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- **Repositorio Remoto y Repositorio Local:**
Los repositorios remotos son versiones de un proyecto que se encuentran almacenados en algún punto de la red, mientras que el Repositorio Local es el que se encuentra alojado en el ordenador en el cual se está trabajando.
[Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

- **Copia de trabajo / Working copy:** 
La copia de trabajo una copia local de los ficheros de un repositorio, en un momento dado. El trabajo realizado sobre los ficheros de un repositorio se realiza sobre una copia de trabajo.
[Wikipedia](https://es.wikipedia.org/wiki/Control_de_versiones)

- **Área de preparación / Starting Area:**
[Agregar Fuente]()

- **Preparar cambios / Stage changes:**
[Agregar Fuente]()

- **Confirmar cambios / Commit changes:**
[Agregar Fuente]()

- **Commit:** 
Confirma cambios realizados en el repositorio local.
[Juristr](https://juristr.com/blog/2013/04/git-explained/)

- **Clone:** Es una copia de un repositorio o la acción como tal de copiar un repositorio, generalmente desde alguna localización remota a un entorno local.
[Linux Academy](https://linuxacademy.com/blog/linux/git-terms-explained/#clone)
[Juristr](https://juristr.com/blog/2013/04/git-explained/)

- **Pull:** 
Es la combinación de los comandos "Fetch" seguido de un "Merge". De este modo se importan confirmaciones y se unen a las ramas, actualizando el repositorio local.
[Git](https://git-scm.com/docs/gitglossary)

* **Push:**
Actualiza la rama remota con las confirmaciones hechas en la rama actual. 
[Linux Academy](https://linuxacademy.com/blog/linux/git-terms-explained/#pull)

- **Fetch:**
Comando utilizado para importar confirmaciones (Commits) desde un repositorio remoto a uno local.
[Atlassian](https://www.atlassian.com/git/tutorials/syncing)

- **Merge:**
Comando que se encarga de unir dos o más ramas. 
[Git](https://git-scm.com/docs/git-merge )

- **Status:**
Comando que se utiliza para determinar el estado en el que se encuentran los archivos.
[Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

- **Log:**
Comando utilizado para mostrar información sobre las confirmaciones realizadas en un repositorio.
[Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

- **CheckOut:**
Este comando es utilizado para saltar de una rama a otra en un repositorio.
[Linux Academy](https://linuxacademy.com/blog/linux/git-terms-explained/#pull)

- **Rama / Branch:**
Un branch es una línea activa de desarrollo. Es posible crear un nuevo branch de a partir de uno existente o cambiar el código independientemente de otros branches. Uno de los branches es el original (generalmente llamado Master). 
[Git](https://git-scm.com/docs/gitglossary)

- **Etiqueta / Tag:** 
Nombre descriptivo dado a determinado “Commit”. También puede contener un mensaje. Generalmente se utilizan para definir que partes de un proyecto Git son más importantes.
[Linux Academy](https://linuxacademy.com/blog/linux/git-terms-explained/#clone)
[Stack Overflow](https://stackoverflow.com/questions/7076164/terminology-used-by-git)




