﻿# Glosario
## Control de versiones (VC)
Con control de versiones nos referiremos a un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos en el paso del tiempo, de modo que se pueda recuperar versiones específicas más adelante cuando se estime conveniente.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

## Control de versiones distribuido (DVC)
Es aquel que permite a muchos desarrolladores de software trabajar en conjunto en un proyecto común sin necesidad de compartir una misma red o estar en el mismo espacio físico.
[Referencia bibliográfica](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

##  Repositorio remoto y Repositorio local
Los repositorios remotos son versiones del proyecto que se encuentran alojados en Internet o en algún punto de la red, por otra parte, los repositorios locales son las copias de estas versiones del proyecto que se disponen para trabajar sobre ellas en un lugar físico.
[Referencia bibliografica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)


## Copia de trabajo / Working Copy
Son una copia de seguridad en caso de necesitar una restauración del sistema debido a una pérdida del servidor.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)


## Área de Preparación / Staging Area
Es un archivo que almacena toda la información acerca de lo que será añadido al repositorio local luego de un commit
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git) 

## Preparar Cambios / Stage Changes
Consiste en notificar a Git que hay archivos que han sido modificados,  y que estos serán incluidos en la próxima confirmación
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
 
## Confirmar cambios / Commit Changes 
Es la confirmación de los cambios que se efectuaran al hacer un commit, estos cambios se mostraran en pantalla al utilizar el comando git status.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## Commit
Sirve para confirmar los cambios realizados, es recomendable incluir un mensaje preciso sobre los cambios.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## Clone
Se utiliza para obtener una copia de un repositorio Git ya existente mientras dispongas del permiso para obtenerlo.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
 
## Pull 
Baja los cambios de la rama determinada y la actualiza contra tu repositorio local.
[Referencia bibliográfica](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git/248)

## Push
Se utiliza para enviar confirmaciones hechas en una rama local a un repositorio remoto.
[Referencia bibliográfica](https://help.github.com/articles/pushing-to-a-remote/)


## Fetch
Se encarga de descargar los cambios de tu repositorio remoto(en el caso de que haya) en una carpeta que se llama  **origin/master**, que es una carpeta oculta.
[Referencia bibliográfica](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git/248)

## Merge
Esta funcionalidad es utilizada para fusionar uno o más ramas dentro de la rama que tienes activa.
[Referencia bibliográfica](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)

## Status 
Es la herramienta principal para determinar el estado de los archivos en el repositorio.
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
## Log
Permite mirar atrás para ver qué modificaciones se han llevado a cabo en el histórico de confirmaciones
[Referencia bibliográfica](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

## Checkout
Permite saltar de una rama a otra utilizando el comando git checkout
[Referencia Bibliográfica](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama?)

## Rama / Branch
 Las ramas son caminos que puede tomar el desarrollo de un software, algo que ocurre naturalmente para resolver problemas o crear nuevas funcionalidades. Se utiliza el comando git branch
 [Referencia bibliográfica](https://desarrolloweb.com/articulos/trabajar-ramas-git.html)

## Etiqueta / Tag
Una etiqueta, o tag, se puede definir como una marca en el histórico de nuestro repositorio. Con estas, podemos marcar nuestro proyecto cuando se lance una nueva versión, por ejemplo. 
Existen 2 tipos: Ligeros y Anotados
[Referencia bibliográfica](https://blog.unelink.es/wiki/como-usar-tags-en-git/)
