﻿# Ejercicio 3
### Cantidad y nombre de los branches
Se dispone de una rama: Master

### Cantidad y nombre de las etiquetas
Hasta el momento no hay ningún tag.

### Nombre, mensaje y código hash de los últimos 3 commits. Use el comando “git log” para obtener esta información.
**Primer Commit**
**Nombre**:
Luis Rojas <luis_felipe_rojas_c@hotmail.com>
**Mensaje** :
Pregunta 4 Definiciones Glosario
Se agregan las definiciones a cada concepto del glosario.
En esta primera entrega se añaden la definición a cada concepto del glosario para la pregunta 4 de la parte 1 del taller.
**Código**:
b9f75aac2fe6edd77188aa8ca04bec79be5bb887
 
**Segundo Commit**
 **Nombre**:
Ismael Figueroa <ismael.figueroa@pucv.cl>
 **Mensaje**:
README.md created online with Bitbucket
 **Código**:
ea8a3aeb490414d1a855689aa0b805c2e623458d
 


