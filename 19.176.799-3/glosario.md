## Glosario ##

- **Control de versiones (VC):** Un sistema que registra los cambios realizados en un archivo para así luego poder recuperar las versiones anteriores cuando se necesiten. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- **Control de versiones distribuido (DVC):** Un sistema que replica completamente el repositorio, en caso de que el servidor muera y se pierdan todos los datos en él. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- **Repositorio remoto y Repositorio local:** El repositorio remoto es el directorio en Github en donde se guarda el contenido. El repositorio local  corresponde al directorio en el ordenador. [Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

- **Copia de trabajo / *Working Copy*:** Una copia del repositorio Git existente, que se crea al clonar el repositorio. [Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

- **Área de Preparación / *Staging Area*:** Un archivo en el repositorio que almacena la información de lo que viene en la siguiente confirmación. [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

- **Preparar Cambios / *Stage Changes*:** Es la preparación que se le hace a un archivo antes de hacer la confirmación y realizar los cambios en el repositorio. [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

- **Confirmar cambios / *Commit Changes*:** Es el paso final donde se realizan los cambios permanentes en el repositorio. [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

- ***commit*:** Realiza la confirmación de los cambios en la cabecera, pero no en el repositorio remoto. [Fuente](https://git-scm.com/docs/git-commit)

- ***clone*:** Copia un repositorio un nuevo directorio. [Fuente](https://git-scm.com/docs/git-clone)

- ***pull*:** Obtiene los cambios de el repositorio remoto y los incorpora al repositorio local. [Fuente](https://git-scm.com/docs/git-pull)

- ***push*:** Incorpora los cambios de el repositorio local al remoto. [Fuente](https://help.github.com/articles/pushing-to-a-remote/)

- ***fetch*:** Obtiene los cambios de el repositorio remoto. [Fuente](https://www.acamica.com/comunidad/4903/cuales-son-las-diferencias-entre-git-pull-y-git-fetch)

- ***merge*:** Incorpora los cambios obtenidos del repositorio remoto al repositorio local. [Fuente](https://www.acamica.com/comunidad/4903/cuales-son-las-diferencias-entre-git-pull-y-git-fetch)

- ***status*:** Muestra el estado de los archivos del repositorio [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

- ***log*:** Muestra el listado de confirmaciones  realizadas sobre el repositorio. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

- ***checkout*:** Crea una nueva rama y se coloca en ella. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

- **Rama / *Branch*:** Es un puntero a cada confirmación que se hace, que se crea cada ver que se confirman cambios. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

- **Etiqueta / *Tag*:** Marcadores que permiten  señalar cambios importantes. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)





