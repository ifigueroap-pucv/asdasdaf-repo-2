﻿# TALLER 0 INGENIERÍA WEB - GLOSARIO
**_a. Control de versiones (VC):_** Un control de versiones es un sistema que guarda los cambios realizados sobre un archivo o varios de ellos, sirve para recuperar versiones en caso que se necesite.
[Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
**_b. Control de versiones distribuido (DVC):_** Sistema que permite a distintas personas trabajar en un mismo archivo, conjunto de archivos o proyecto.
[Git - Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) 
**_c. Repositorio remoto y Repositorio local:_** Remoto: Es el proyecto que está en internet Local: Es el que está alojado en nuestro computador.
[Fundamentos de Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
**_d. Copia de trabajo / Working Copy:_** Es la carpeta de nuestro proyecto y con lo que se ve dentro de esta carpeta, es lo que podemos modificar.
 [Las tres zonas de Git](https://www.youtube.com/watch?v=mVjHJFscwsk)
**_e. Área de Preparación / Staging Area:_** Es donde vamos a poner los archivos de los cuales queremos ir guardando versiones de ellos. 
[Las tres zonas de Git](https://www.youtube.com/watch?v=mVjHJFscwsk)
**_f. Preparar Cambios / Stage Changes:_** Después de modificar los archivos, se le avisa a Git que estos archivos serán incluidos en la proxima actualizacion. 
[Staging the changes](https://githowto.com/staging_changes)
**_g. Confirmar cambios / Commit Changes:_** Confrima los cambios que fueron agregados al Staging Area, para agregarlos al repositorio local.
[Fundamentos de Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios)
**_h. Commit:_** Comando para confirmar cambios en el repositorio local.
[Fundamentos de Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios)
**_i. clone:_** Sirve para obtener una copia de un repositorio en cual quiero colaborar 
[Fundamentos de Git - Obteniendo un repositorio Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
**_j. pull:_** Se utiliza este comando para juntar todos los cambios que se hicieron una rama determinada y  mete en el repositorio local es una combinacion de un fetch y un merge.
[Diferencia entre Pull y Fetch en Git](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)
**_k. push:_** Comando que permite enviar al repositorio remoto los cambios realizados y confimados en el local.
[Fundamentos de Git - Trabajando con repositorios remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
**_l. fetch:_**  Comando para actualizar cambios del repositorio remoto al local. 
[Fundamentos de Git - Trabajando con repositorios remotos](ttps://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos#Recibiendo-de-tus-repositorios-remotos)
**_m. merge:_** Comando que se utiliza para juntar 2 o mas historiales de desarrollo juntos. 
[Git - Merge](https://git-scm.com/docs/git-merge)
**_n. status:_** Comando que se utiliza para mostrar el estado actual de los archivos de un repositorio. 
[Fundamentos de Git - Guardando cambios en el repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Comprobando-el-estado-de-tus-archivos)
**_o. log:_** Comando que se utiliza para ver el historial de las modificaciones que se han llevado a cabo en el repositorio.
[Fundamentos de Git - Viendo el historico de confirmaciones](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-histórico-de-confirmaciones)
**_p. checkout:_** Comando que se utiliza para cambiar ramas o crear ramas, se utilizar para aislar posibles problemas.
[Ramificaciones en Git - Procedimientos basicos para ramificiar y fusionar](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-básicos-para-ramificar-y-fusionar)
**_q. Rama / Branch:_** Es una linea de trabajo. Comando: Se usa para borrar, crear o listar ramas.
[Comandos Basicos Git](https://www.hostinger.es/tutoriales/comandos-de-git)
**_r. Etiqueta / Tag:_** Punto del historial de cambios del proyecto, se usan cuando se saca una nueva actualizacion o version. Por ejemplo, v1.4. 
[Fundamentos de Git - Creando Etiquetas](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)

