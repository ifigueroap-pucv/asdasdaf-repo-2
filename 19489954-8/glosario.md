﻿# Taller 0 Ingeniería Web!

### a. Control de versiones (VC): 
El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante.
[Fuente A](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

### b. Control de versiones distribuido (DVC):
En un DVCS (como Git, Mercurial, Bazaar o Darcs), los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio. Así, si un servidor muere, y estos sistemas estaban colaborando a través de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo. Cada vez que se descarga una instantánea, en realidad se hace una copia de seguridad completa de todos los datos.
[Fuente B](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

### c. Repositorio remoto y Repositorio local:
Los **repositorios remotos** son versiones de tu proyecto que se encuentran alojados en algún punto de la red. Puedes tener varios repositorios remotos, cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas. Nos sirve crear un repositorio remoto en el caso es que se trabaje con varias personas para que ellos también puedan acceder a los contenidos.
Un **repositorio local** es en donde se localiza tus trabajos en tu misma red o tu mismo servidor.
[Fuente C](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

### d. Copia de trabajo / Working Copy:
Una **copia de trabajo** se le llama a la clonación de un repositorio cuando el git recibe una copia de casi todos los datos que tiene el servidor, se crea la copia de todos los historiales de los archivos que se encuentra en nuestro servidor.
[Fuente D](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

### e. Área de Preparación / Staging Area: 
El **área de preparación** es en donde pondremos los archivos donde queremos guardar próxima versiones. 
En algunas ocasiones nos podemos saltar el área de preparación pasando -a al comando ***git commit***, esto hace que Git prepare todo archivo que estuviese en seguimiento antes d la confirmación permitiéndote obviar la parte del ***git add***.
[Fuente E.1](https://www.youtube.com/watch?v=mVjHJFscwsk)
[Fuente E.2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

### f. Preparar Cambios / Stage Changes:
La **preparación de cambios** es cuando uno tiene listo el archivo que deseaba modificar y es subido al área de preparación. Lo bueno de esto es que nosotros lo podemos probar antes que sea confirmado el cambio, es decir no es permanente en el repositorio antes del commit changes.
[Fuente F](https://githowto.com/staging_changes)

### g. Confirmar cambios / Commit Changes: 
Luego de que ya se ha pasado por el stage changes y la staging area, se realiza un **commit changes** para confirmar los cambios que deseábamos modificar.
[Fuente G](https://githowto.com/staging_and_committing)

### h. Commit:
El comando commit es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto.
[Fuente H](https://www.hostinger.es/tutoriales/comandos-de-git)

### i. clone:
Este comando clone se usa con el propósito de revisar repertorios.
[Fuente I](https://www.hostinger.es/tutoriales/comandos-de-git)

### j. pull:
El comando Pull nos sirve para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando
[Fuente J](https://www.hostinger.es/tutoriales/comandos-de-git)


### k. push: 
Este es uno de los comandos más básicos. Un simple push envía los cambios que se han hecho en la rama principal de los repertorios remotos que están asociados con el directorio que está trabajando.
[Fuente K](https://www.hostinger.es/tutoriales/comandos-de-git)
### l. fetch:
Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que está trabajando.
[Fuelte L](https://www.hostinger.es/tutoriales/comandos-de-git)
### m. merge: 
El comando merge se usa para fusionar una rama con otra rama activa.
[Fuente M](https://www.hostinger.es/tutoriales/comandos-de-git)
### n. status: 
El comando status muestra la lista de los archivos que se han cambiado junto con los archivos que están por ser añadidos o comprometidos.
[Fuente N](https://www.hostinger.es/tutoriales/comandos-de-git)
### o. log:
El comando log nos muestra una lista de commits en una rama junto con todos los detalles.
[Funte O](https://www.hostinger.es/tutoriales/comandos-de-git)
### p. checkout: 
El comando checkout se puede usar para crear ramas o cambiar entre ellas.
[Fuente P](https://www.hostinger.es/tutoriales/comandos-de-git)
### q. Rama / Branch: 
Este comando se usa para listar, crear o borrar ramas.
[Fuente Q](https://www.hostinger.es/tutoriales/comandos-de-git)
### r. Etiqueta / Tag:
Etiquetar se usa para marcar commits específicos con asas simples.
[Fuente R](https://www.hostinger.es/tutoriales/comandos-de-git)




