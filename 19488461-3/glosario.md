# Glosario

**1.- Control de versiones (VC):**
		El control de versiones es una forma de respaldar los cambios hechos a un proyecto. Esto permite deshacer alg�n cambio (volviendo a una versi�n anterior)  que pueda perjudicar el avance logrado. Esto se logra manteniendo un control de cu�ndo y qu� se ha modificado.

[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)

**2.-Control de versiones distribuido (DVC):**
		Sistema que permite trabajar un proyecto en conjunto, con la posibilidad de que cada usuario pueda replicar el repositorio completo en caso de un fallo o muerte del servidor.

[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

**3.-Repositorio remoto y Repositorio local:**
	Un repositorio remoto es el repositorio que se guarda en internet, mientras que el repositorio local es el que se guarda en un directorio del computador del usuario.
	[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
	
**4.-Copia de trabajo / Working Copy:**
Al querer trabajar en el proyecto ese necesario crear una copia de trabajo del repositorio, es sobre esta donde el usuario va a realizar con los cambios que estime conveniente.
[Fuente](https://www.osmosislatina.com/cvs_info/wcopy.htm)

**5.-�rea de Preparaci�n / Staging Area:** 
Es un archivo que mantiene la informaci�n de lo que va a ser a�adido al repositorio local cuando se haga commit.

[Fuente](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva)

**6.-Preparar Cambios / Stage Changes:**
Se notifica a Git que sobre la existencia de archivos modificados que ser�n incluidos en la siguiente confirmaci�n.

[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**7.-Confirmar cambios / Commit Changes:**
Se realizan los cambios hechos a los archivos en el repositorio, es el paso final para guardar las modificaciones.

[Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

**8.-Commit:**
Este comando confirma los cambios hechos a los archivos en el repositorio local.

[Fuente](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

**9.-clone:**
Copia un repositorio Git a un directorio.

[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

**10.-pull:**
 Obtiene los cambios de el repositorio remoto y los actualiza a un repositorio local.

 [Fuente](https://git-scm.com/docs/git-pull)

**11.-push:**
Este comando se usa para enviar confirmaciones hechas en una rama local al repositorio remoto.

[Fuente](https://help.github.com/articles/pushing-to-a-remote/)

**12.-fetch:**
 Este comando descarga las modificaciones que no tengas desde repositorio remoto a la rama origin/master. No modifica la informaci�n en la cual se esta trabajando, esto se debe realizar posteriormente.
  
  [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b�sicos-para-ramificar-y-fusionar)

**13.-Merge:**
Esto es utilizado para fusionar una o m�s ramas dentro de la rama que est� activa.

[Fuente](https://git-scm.com/book/es/v2/Appendix-C:-Comandos-de-Git-Ramificar-y-Fusionar)

**14.-status:**
 
 Comando que muestra el estado de los archivos de un repositorio.
 
 [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
 
**15.-log:** 
Comando que permite ver el hist�rico de confirmaciones, con sus respectivas modificaciones en orden cronol�gico inverso.

[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist�rico-de-confirmaciones)

**16. checkout:**
Crea una nueva rama y se posiciona en ella.

[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b�sicos-para-ramificar-y-fusionar)

**17.-Rama / Branch:** 
Puntero a cada confirmaci�n que se hace, permiten que el proyecto tenga m�s de un estado, y que el usuario pueda saltar de uno a otro seg�n sea conveniente.

 [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**18.-Etiqueta / Tag:**
Se le llama tag, o etiqueta, a una marca en el hist�rico de nuestro repositorio. Con esta se puede marcan momentos importantes de nuestro proyecto, como el lanzamiento de una versi�n, por ejemplo.

[Fuente](https://blog.unelink.es/wiki/como-usar-tags-en-git/)
